<?php
use Illuminate\Http\Request;
use App\Exports\UserExportData;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProductsExport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/excel-general', function () {
    return Excel::download(new ProductsExport, 'userGeneric.csv');
});

Route::get('scraping','ScrapingStudyPortalsController@getAllCourses');
Route::get('scraping_master','ScrapingMasterController@getAllCourses');
Route::get('scraping_phd','ScrapingPhdController@getAllCourses');
Route::get('scraping_edx','ScrapingEdxController@index');
Route::get('scraping_platzi','ScrapingPlatziController@updateCourses');
Route::get('platzi','ScrapingPlatziController@index');


Route::get('charge_institutions_url','GeneralController@chargeInstitutionsURL');