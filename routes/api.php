<?php

use Illuminate\Http\Request;
use App\Exports\UserExportData;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/excel', function () {
    return Excel::download(new UserExportData, 'userdata.xlsx');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::resource('states', 'StatesController');

Route::resource('cities', 'CitiesController');

Route::get('cities/read_limit/{id}', 'CitiesController@readLimit');

Route::get('cities/city_by_state/{id}/edit', 'CitiesController@cityByState');

Route::resource('countries', 'CountriesController');

Route::resource('courses', 'CoursesController');

Route::get('courses_institutions/{id}','CoursesController@coursesInstitution');

Route::post('courses_all','CoursesController@getAllCourses');

Route::resource('courses_details', 'CoursesDetailController');

Route::resource('disciplines', 'DisciplinesController');

Route::get('disciplines_type/{id}','DisciplinesController@getDisciplineForType');

Route::resource('higher_education', 'HigherEducationController');

Route::resource('history', 'HistoryController');

Route::resource('inst_profiles', 'InstProfilesController');


//*********************institutiuons***************************************
Route::resource('institutions', 'InstitutionsController');

Route::post('institutions_count', 'InstitutionsController@getCountInstitutions');
Route::post('institution_update_width_image', 'InstitutionsController@institutionUpdateWidthImage');

Route::post('institutions/read_limit', 'InstitutionsController@readLimit');

//**************************************************************************

Route::resource('locations', 'LocationsController');

Route::resource('score_courses', 'ScoreCoursesController');

Route::resource('score_disciplines', 'ScoreDisciplinesController');

Route::resource('score_institutions', 'ScoreInstitutionsController');

Route::resource('user_data', 'UserDataController');

Route::resource('favorites', 'FavoriteController');


/* detail user */

Route::post('user_data/previous_studies','UserDataController@createPreviousStudes');
Route::get('user_data/previous_studies/{id}','UserDataController@readPreviousStudes');
Route::delete('user_data/previous_studies/{id}','UserDataController@deletePreviousStudes');

Route::post('user_data/language_user','UserDataController@createLanguageUser');
Route::get('user_data/language_user/{id}','UserDataController@readLanguageUser');
Route::delete('user_data/language_user/{id}','UserDataController@deleteLanguageUser');

Route::post('user_data/work_experience','UserDataController@createSearchingDetalle');
Route::get('user_data/work_experience/{id}','UserDataController@readSearchingDetalle');
Route::delete('user_data/work_experience/{id}','UserDataController@deleteSearchingDetalle');

Route::post('user_data/test_user','UserDataController@createTestUser');
Route::get('user_data/test_user/{id}','UserDataController@readTestUser');
Route::delete('user_data/test_user/{id}','UserDataController@deleteTestUser');

Route::post('user_data/imagen_perfil','UserDataController@imagenPerfil');


Route::post('user_data/login','UserDataController@authenticate');
Route::get('logout','UserDataController@logout');
Route::post('valida-caducidad-token','UserDataController@validaCaducidadToken');

Route::post('user_data/loginRedSociale','UserDataController@loginRedSociale');
Route::post('user_data_univercity','UserDataController@userDataUniverscity');

Route::post('user_exports','UserDataController@dataForExportUsers');

 /* Finish */

//Route::resource('work_experience', 'WorkExperienceController');

// General controller
Route::get('sendWelcomeEmail/{email}/{firstName}', 'GeneralController@sendWelcomeEmail');
Route::post('sendContactEmail', 'GeneralController@sendContactEmail');
Route::post('helpMeToApplyEmail', 'GeneralController@helpMeToApplyEmail');


// Scraping

Route::resource('scraping_study_portals','ScrapingStudyPortalsController');
Route::get('scraping','ScrapingStudyPortalsController@getAllCourses');

Route::get('master','ScrapingMasterController@index');
Route::get('scraping_master','ScrapingMasterController@getAllCourses');

Route::get('phd','ScrapingPhdController@index');
Route::get('scraping_phd','ScrapingPhdController@getAllCourses');


Route::get('scraping_edx','ScrapingEdxController@index');

Route::get('scraping_detail_bachelor','ScrapingDetailBachelorController@index');
Route::get('scraping_detail_master','ScrapingDetailMasterController@index');
Route::get('scraping_detail_phd','ScrapingDetailPhdController@index');

Route::get('scraping_edx','ScrapingEdxController@index');

//
Route::get('platzi','ScrapingPlatziController@index');
Route::get('scraping_platzi','ScrapingPlatziController@updateCourses');
//Formulario search cursos

Route::post('curso-search','CursoSearchController@index');
/**
 * Info de dashboard
 */
Route::get('contarUsuarios','UserDataController@contarUsuarios');
Route::get('listadoUsuariosMeses','UserDataController@listadoUsuariosMeses');

Route::get('contarInstituciones','InstitutionsController@contarInstituciones');
Route::get('contarCursos','CoursesController@contarCursos');


Route::group(['middleware' => ['jwt.verify']], function() {
   
});

Route::get('generar-reporte-general','ReportsController@generarReporteGeneral');

Route::get('imprimir','ReportsController@imprimir');

Route::post('excel-general','ReportsController@collection');

// Route::get('/excel-general', function () {
//     return Excel::download(new ProductsExport, 'userGeneric.csv');
// });