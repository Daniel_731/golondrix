<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- Custom CSS -->
        <style>

            body {
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            }

            @media screen and (min-width: 576px) {
                .container {
                    max-width: 540px;
                }
            }

            @media screen and (min-width: 768px) {
                .container {
                    max-width: 720px;
                }
            }

            @media screen and (min-width: 992px) {
                .container {
                    max-width: 960px;
                }
            }

            @media screen and (min-width: 1200px) {
                .container {
                    max-width: 1140px;
                }
            }

            .container {
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            .jumbotron {
                padding: 2rem !important;
                background-color: #E9ECEF;
                border-radius: .3rem;
            }

            .display-4 {
                font-size: 3.5rem;
                font-weight: 300;
                line-height: 1.2;
            }

            .text-center {
                text-align: center!important;
            }

            .my-4 {
                margin-bottom: 1.5rem !important;
                margin-top: 1.5rem !important;
            }

            hr {
                border: 0;
                border-top: 1px solid #0000001A;
                box-sizing: content-box;
                height: 0;
                overflow: visible;
            }

            .lead {
                font-size: 1.25rem;
                font-weight: 300;
            }

            p {
                margin-top: 0;
                margin-bottom: 1rem;
            }

            *, ::after, ::before {
                box-sizing: border-box;
            }

            .mt-0 {
                margin-top: 0 !important;
            }

            .mb-0 {
                margin-bottom: 0 !important;
            }

        </style>

        <title></title>

    </head>


    <body>
        <div class="container">
            <div class="jumbotron">
                <h1 class="display-4 text-center mt-0">Help me to apply</h1>

                <hr class="my-4"/>

                <h3 class="mb-0">Name</h3>
                <p>{{ $name }}</p>

                <h3 class="mb-0">E-mail</h3>
                <p>{{ $email }}</p>

                <h3 class="mb-0">Nationality</h3>
                <p>{{ $nationality }}</p>

                <h3 class="mb-0">Course name</h3>
                <p>{{ $course }}</p>

                <h3 class="mb-0">Institution</h3>
                <p>{{ $institution }}</p>

                <h3 class="mb-0">What are your main interests?</h3>
                <p>{{ $interests }}</p>
            </div>
        </div>
    </body>

</html>