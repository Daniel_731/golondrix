<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_institution
 * @property int $score
 * @property string $comment
 * @property string $date_time
 * @property int $id_client
 * @property Institution $institution
 */
class ScoreInstitutions extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_institution', 'score', 'comment', 'date_time', 'id_client'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function institution()
    {
        return $this->belongsTo('App\Institution', 'id_institution', 'id_institution');
    }
}
