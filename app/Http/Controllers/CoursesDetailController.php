<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\CoursesDetails;

class CoursesDetailController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return void
	 */
	public function index()
	{
    	//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return void
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return void
	 */
	public function store(Request $request)
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return JsonResponse
	 */
	public function edit($id)
	{
		$cursos = DB::table('courses_details')
			->where('courses_details.id_curso1', '=', $id)
			->join('courses', 'courses.id_course', '=', 'courses_details.id_curso1')
			->get();

		return response()->json([
			"courses_details" => $cursos
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return void
	 */
	public function update(Request $request, $id)
	{
		CoursesDetails::find($id)->update($request->all());
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return void
	 */
	public function destroy($id)
	{
		//
	}
}
