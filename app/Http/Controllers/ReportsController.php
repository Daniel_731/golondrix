<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    function imprimir(){
    	$pdf = \PDF::loadView('reports/report1');
    	return $pdf->stream();
    }

    public function collection(Request $request)
    {
        $reporte = DB::select(
            "SELECT  ud.first_name AS firstName, 
            ud.last_name AS lAStName,
            ud.user_name AS name, 
            ud.birth_day AS birthDay, 
            ud.email AS email, 
            ud.gender AS gender,
            ud.study_year AS studyYear,  
            ud.created_at AS created_at,
            coh.name_course AS courseHistory,
            coh.type AS typeCourses,
            coh.duration AS duration,
            dic.name_discipline AS nameDiscipline,
            inst.name_institution AS nameInstitution,
            courd.url_university AS urlUniversity,
            cof.name_course AS courseFavorite,
            coh.country AS countriesName, 
            coh.city AS cityName
            from user_data ud 
            LEFT JOIN favorite fa ON   ud.id  = fa.id_user
            LEFT JOIN history hy ON  ud.id  = hy.id_user
            LEFT JOIN courses coh ON  coh.id_course = hy.id_user
            LEFT JOIN disciplines dic ON   coh.id_discipline = dic.id_discipline
            LEFT JOIN institutions inst ON  coh.id_institution = inst.id_institution
            LEFT JOIN courses_details courd ON courd.id_curso1 = coh.id_course 
            LEFT JOIN countries cout ON ud.id_location  =  cout.id 
            LEFT JOIN courses cof ON cof.id_course = coh.id_course
            LEFT JOIN favorite fac ON  cof.id_course = fac.id_course"
        );

        return response()->json([
			"status" => true,
			"data" => $reporte
		]);
    }

}
