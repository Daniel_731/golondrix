<?php

namespace App\Http\Controllers;

use App\{Disciplines, Courses,CoursesDetails};
use Goutte\Client;

class ScrapingPlatziController extends Controller
{
    
    public $a;
    public $crawler;
    public $discipline;
    public $idCurso;
    public $url;

    
    public function index()
    {
        $this->disciplinese = array();
        $this->link = array();
        $this->array1 = array();
        $this->contador = 0;

      
 
            $client = new Client();
            $this->crawler = $client->request('GET', 'https://courses.platzi.com/');
            $this->discipline = new Disciplines();
            $courseList = $this->crawler->filter(" [class='CourseList']")->each(function ($nodeCourseList) {
           
               $courseAll = $nodeCourseList->filter("[class='Career']")->each(function( $nodeLinkCourses){

                    $img= $nodeLinkCourses->filter("[class='Career-header']")->each(function( $nodeImg){
                        $img1= $nodeImg->filter("[class='Career-headerPrimary']")->each(function( $nodeImg1){
                            $img2= $nodeImg1->filter("img")->each(function( $nodeImg2){
                                $this->discipline = new Disciplines();
                                $this->discipline->id_institution=200;
                                $this->discipline->name_discipline = $nodeImg2->attr('alt');
                                $this->discipline->type = "Platzi";
                                $this->discipline->method = "online";
                                $this->discipline->url =$nodeImg2->attr('src');
                                $this->discipline->save();
                                    
                            });
                            
                                
                        });
                       
                        
                    }); 
                    
                    $img3= $nodeLinkCourses->filter("[class='Career-body']")->each(function( $nodeImg2){
                        $img4= $nodeImg2->filter("[class='Career-list']")->each(function( $nodeImg3){
                            $img5= $nodeImg3->filter("a")->each(function( $nodeImg4){
                                $course = new Courses();
                                $getDisciplines = Disciplines::all();
                                $getDiscipline = $getDisciplines->last();
                                $idDiscipline =$getDiscipline->id_discipline;

                                $course->id_discipline = $idDiscipline;
                                $course->name_course = $nodeImg4->text();
                                $course->type = "Platzi";
                                $course->url = $nodeImg4->attr('href');
                                $course->density_parttime = 1;
                                $course->methods_online = 1 ;
                                $course->description_course = '' ;
                                $course->id_institution = 200;
                                $course->tuition_fee_currency = "USD";
                                $course->tuition_fee_unit = "full";

                                 $course->save();
                                
                                    
                            });
                            
                                
                        });
                       
                        
                    });
                    
                    

                });
        
         

       });
 
    }

    public function updateCourses(){

        try {
          
            $client = new Client();
            $url="";

            $allcourses = Courses::where("type","=","Platzi")->get();
            echo "1";
            foreach($allcourses as $all){
                $this->idCurso =$all->id_course;
                $this->url = "https://courses.platzi.com$all->url";
                $this->crawler = $client->request('GET', $this->url );
                echo "2";
                $div = $this->crawler->filter("[class='LandingEnglish']")->each(function($nodeDiv){
                    echo "3";
                    $description = $nodeDiv->filter("[class='p BannerTop-description']")->text();
                    $courseDetail = new CoursesDetails();
                    echo "4";
                    $courseDetail->id_curso1 = $this->idCurso;
                    
                    $courseDetail->full_description=$description;
                    echo "5";
                    $courseDetail->language = "english";
                    echo ".....";
                    $courseDetail->url_university =$this->url;
                    echo "6";
                    $courseDetail->save();
                    echo "7";
                    
                });

                // LandingEnglish

                // p BannerTop-description

                // EnglishMainCTA-price

            }
           
            
            
           

        } catch (\Throwable $th) {
         
        }
    }

    
}
