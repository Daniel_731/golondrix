<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ScrapingDetailMasterController;
use Goutte\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as ClientGuzzle;
use App\{Cities, Disciplines, Courses, Institutions};

class ScrapingMasterController extends Controller
{

    public $links;
    public $textCourse;
    public $linkCourses;
    public $totalLinks;
    public $idDiscipline;


    public function index(){
        
      $client = new Client();
      $this->disciplines = array();
      $this->links = array();

      $crawler = $client->request('GET', 'https://www.mastersportal.com/');
      $crawler->filter(" [id='DisciplineSpotlight']")->each(function ($nodeDiscipline) {

         $courses = $nodeDiscipline->children()->filter("[data-clickable='clickable']")->each(function ($nodeCourses) {

            $a = $nodeCourses->children()->filter("a")->each(function ($nodeA) {
               $this->links = $nodeA->attr('href');//Se tre el link para accder a el titulo y la descripción de la disciplina
            });

            $array1 = array(
               'discipline' => $nodeCourses->text(),
               'links' => $this->links
            );

            array_push($this->disciplines, $array1);
         });
      });

      $this->getScrapingCourseOfDiscipline($this->disciplines);
    }

    public function getScrapingCourseOfDiscipline($disciplineSelected)
   {

      $client2 = new Client();
      $this->textCourse = '';
      $this->linkCourses = '';
      $this->totalLinks = array();

      foreach ($disciplineSelected as $dis) {

         $this->idDiscipline = explode("/", $dis['links'])[4];
         $crawler2 = $client2->request('GET', $dis['links']);
         $this->textCourse = $crawler2->filter("[class='Module StudyPortals_Shared_Modules_Discipline_HeaderBase_HeaderBase StudyPortals_Masters_Modules_Discipline_Header_HeaderMasters']");

         $link = $crawler2->filter("[class='DisciplineConversion']")->each(function ($nodeLinks) {
            $a = $nodeLinks->children()->filter("a")->each(function ($nodeA) {
               $this->totalLinks = array(
                  'total' => $nodeA->attr('href')
               );
            });
         });

         $crawler2->filter(" [class='Module StudyPortals_Shared_Modules_Discipline_DisciplineDescriptionRR_DisciplineDescriptionRR']")->each(function ($nodeDiscipline) {

            $courses = $nodeDiscipline->children()->filter("[id='DisciplineDescription']")->each(function ($nodeCourses) {
               $discipline = new Disciplines();
               $discipline->id_real = $this->idDiscipline;
               $discipline->name_discipline = $this->textCourse->text();
               $discipline->type =  "Master";
               $discipline->description_discipline = $nodeCourses->text();
               $discipline->save();//se guardan los datos de las disciplinas en la BD
            });
         });

      }
   }



   public function getAllCourses()
   {

      try {
         // echo "1";
         ini_set("memory_limit", "2048M");
         ini_set("max_execution_time", 20000);

         $all_courses = array();
         $response = null;
         $id_discipline = null;
         
         $client = new ClientGuzzle([
            'headers' => ['content-type' => 'application/json', 'Accept' => 'application/json'],
            
         ]);
         
         $allDisciplines = Disciplines::where("type","=","Master")->get();
         
         foreach ($allDisciplines as $discipline) {
  
            $id_discipline = $discipline->id_real;
             
            //  echo "++".$id_discipline;
            $i = 0;

             while(true){
              
               try {

                  $i += 10;

                  try {

                     $response = $client->request("GET", "https://search.prtl.co/2018-07-23/?start=$i&q=di-$id_discipline%7Cen-2019%7Clv-master%7Ctc-USD%7Cuc-88");
                      // echo".....................".$i;
                     $all_courses = json_decode($response->getBody());

                  } catch (\Exception $e) {
                     break;
                   }

                  if ($response->getStatusCode() == 400 || empty($all_courses) || !isset($all_courses) || $response->getStatusCode() == false) {
                     break;
                  }

                  foreach ($all_courses as $course) {
                     try {

                        // echo $response->getStatusCode() ." - ". (isset($course->title) ? $course->title : "No tiene titulo") . "<br />";

                        $discipline2 = Disciplines::where("id_real","=",$id_discipline)->where("type","=","Master")->first();

                        if (isset($course->organisation_id)) {
                           $institution = Institutions::where("organisation_id", "=", $course->organisation_id)->first();
                        }

                        if (isset($course->venues[0]->city)) {
                           $city = Cities::where("name" , "=", $course->venues[0]->city)->first();
                        }

                        if ($institution == null) {
                           $institution = new Institutions();

                           if(isset($course->organisation_id)){
                              $institution->organisation_id = $course->organisation_id;
                              }
                              
                              if(isset($course->organisation)){
                                 $institution->name_institution = $course->organisation;
                              }
                              
                              if(isset($course->logo)){
                                 $institution->url = $course->logo;
                              }
                              
                              if(isset($course->summary)){
                              $institution->description_institution = $course->summary;   
                              }
                              $institution->save();
                        }


                        $course_create = new Courses();

                        $course_create->id_discipline = isset($discipline2->id_discipline) ? $discipline2->id_discipline : null;
                        $course_create->id_institution = isset($institution->id_institution) ? $institution->id_institution : null;
                        $course_create->id_city = isset($city->id) ? $city->id : null;

                        $course_create->name_course = isset($course->title) ? $course->title : null;
                        $course_create->description_course = isset($course->summary) ? $course->summary : null;
                        $course_create->type = "Master";
                        $course_create->duration = isset($course->fulltime_duration->value) ? $course->fulltime_duration->value : null;
                        $course_create->url = isset($course->logo) ? $course->logo : null;
                        $course_create->density_parttime = isset($course->density->parttime) ? $course->density->parttime : null;
                        $course_create->density_fulltime = isset($course->density->fulltime) ? $course->density->fulltime : null;
                        $course_create->methods_face2face = isset($course->methods->face2face) ? $course->methods->face2face : null;
                        $course_create->methods_online = isset($course->methods->online) ? $course->methods->online : null;
                        $course_create->methods_blended = isset($course->methods->blended) ? $course->methods->blended : null;
                        $course_create->tuition_fee_value = isset($course->tuition_fee->value) ? $course->tuition_fee->value : null;
                        $course_create->tuition_fee_unit = isset($course->tuition_fee->unit) ? $course->tuition_fee->unit : null;
                        $course_create->tuition_fee_currency = isset($course->tuition_fee->currency) ? $course->tuition_fee->currency : null;
                        $course_create->codigo_curso = isset($course->id) ? $course->id : null;
                        $course_create->city = isset($course->venues[0]->city) ? $course->venues[0]->city : null;
                        $course_create->country = isset($course->venues[0]->country) ? $course->venues[0]->country : null;
                        $course_create->area = isset($course->venues[0]->area) ? $course->venues[0]->area : null;

                        $course_create->save();
                        // $detail = new ScrapingDetailMasterController();
                        // $detail->index($course_create->id_course,$course_create->codigo_curso);

                     } catch (\Exception $e) {
                        return response()->json([
                           "status" => false,
                           "error1" => $e->getMessage()
                        ]);
                     }
                  }

                   // echo /*$response->getBody() . */"<br /><br /><br />";

               } catch (\Exception $e) {
                  return response()->json([
                     "status" => false,
                     "error2" => $e->getMessage()
                  ]);
               }
            }
         }


      } catch (\Exception $e) {
          return response()->json([
              "status" => false,
              "error2" => $e->getMessage()
          ]);
      }

   }
}