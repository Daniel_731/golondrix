<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use GuzzleHttp\Client as ClientGuzzle;
use App\{Courses, CoursesDetails, Disciplines};

class ScrapingDetailBachelorController extends Controller
{

	public $coursesDetails;
    
	public function index() {
		try {

			ini_set("memory_limit", "8000M");
			ini_set("max_execution_time", 200000);


			$disciplines = Disciplines::where("type", "=", "Phd")->get();

         foreach($disciplines as $discipline) {

				$id_discipline = $discipline->id_discipline;

				$courses = Courses::leftJoin("courses_details", "courses.id_course", "=", "courses_details.id_curso1")
					->where("id_discipline", "=", $id_discipline)
					->where("courses_details.id_curso1", "=", null)
					->orderBy("codigo_curso")
					->get();

				$courses_count = $courses->count();
				$counter = 0;
				$counter_print = 0;

				// dd($courses_count);

				$all_courses_details = null;


				while ($courses_count > 0) {

					$courses_url = "";

					$courses_slice = array_slice($courses->all(), $counter, 100);
					$counter += 100;
					$courses_count -= 100;

					foreach ($courses_slice as $course_slice) {
						if ($courses_url !== "") {
							$courses_url .= "%2C";
						}

						$courses_url .= $course_slice->codigo_curso;
					}

					$client = new ClientGuzzle(["headers" => ["content-type" => "application/json", "Accept" => "application/json"]]);

					switch($discipline->type) {
						case "Master":
							$url = "".
								"https://reflector.prtl.co/?".
								"length=100".
								"&token=6244629c4eb9b4dfdcd06f6f514907137b54e6f6".
								"&q=id-".$courses_url.
								"&path=data%2Fstudies%2Fany%2Fdetails%2F";
							break;

						default:
							$url = "".
								"https://reflector.prtl.co/?".
								"length=100".
								"&token=6244629c4eb9b4dfdcd06f6f514907137b54e6f6".
								"&q=id-".$courses_url.
								"&path=data%2Fstudies%2Fpublic%2Fdetails%2F";
							break;
					}

					$response = $client->request("GET", $url);
					
					$all_courses_details = json_decode($response->getBody());

					foreach($all_courses_details as $course_details) {
						try {

							$course = Courses::where("id_discipline", "=", $id_discipline)->where("codigo_curso", "=", $course_details->id)->first();

							$course_details_db = new CoursesDetails();

							$course_details_db->id_curso1 = $course->id_course;
							$course_details_db->full_description = $course_details->description;
							$course_details_db->requirements = $course_details->requirements;
							$course_details_db->fulltime_duration = $course_details->fulltime_duration;
							$course_details_db->duration_period = $course_details->fulltime_duration_period;

							foreach ($course_details->link_homepage as $links) {
								$course_details_db->url_university = $links->url;
							}

							$course_details_db->save();

						} catch (\Exception $e) {}
					}

					$all_courses_details = null;
				}


				echo "Registros creados correctamente";
			}

		} catch (\Exception $e) {
			echo "Error: " . $e->getMessage();

			$this->index();
		}
   }
}

