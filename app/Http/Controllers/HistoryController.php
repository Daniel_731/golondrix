<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
      //
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
      try {

         $history = new History();

         $history->id_user = $request->all()['id_user'];
         $history->id_course = $request->all()['id_course'];

         $history->save();


         return response()->json([
            "status"     => true,
            "id_history" => $history->id_history
         ]);

      } catch (\Exception $e) {
         return response()->json([
            "status" => false,
            "error" => $e->getMessage()
         ]);
      }
   }

   /**
    * Display the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      try {

         $history = History::query()
            ->select(
               'history.id_history',
               'history.updated_at',

               'institutions.name_institution',
               "institutions.id_institution",

               'courses.name_course',
               'courses.url',
               'courses.description_course',
               'courses.type',
               'courses.tuition_fee_value',
               'courses.tuition_fee_currency',
               'courses.tuition_fee_unit',
               'courses.density_fulltime',
               'courses.density_parttime',
               'courses.duration',
               'courses.methods_face2face',
               'courses.methods_online',
               'courses.methods_blended',

               "courses_details.url_university",
               "courses_details.id_curso1"
            )
            ->join('courses', 'courses.id_course', '=', 'history.id_course')
            ->join('institutions', 'institutions.id_institution', '=', 'courses.id_institution')
            ->leftJoin('courses_details', 'courses.id_course', '=', 'courses_details.id_curso1')
            ->where('history.id_user', '=', $id)
            ->orderBy('history.updated_at', 'DESC')
            ->get();


         return response()->json([
            "status" => true,
            "histories" => $history
         ]);

      } catch (\Exception $e) {

         return response()->json([
            "status" => false,
            "error" => $e->getMessage()
         ]);
      }
   }

   /**
    * Update the specified resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
      //
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      try {

         History::find($id)->delete();

         return response()->json([
            "status" => true
         ]);

      } catch (\Exception $e) {

         return response()->json([
            "status" => false,
            "error" => $e->getMessage()
         ]);
      }
   }
}
