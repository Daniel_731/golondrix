<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use App\{Courses, CoursesDetails};

class ScrapingDetailMasterController extends Controller
{

   public $coursesDetails;

   public function index()
   {
      try {
         ini_set("memory_limit", "2048M");
         ini_set("max_execution_time", 10000);

         $client = new Client();


         $client = new Client();
         $courses = Courses::where('type',"=","Master")->get();

         foreach($courses as $course) {
            $this->coursesDetails = new CoursesDetails();
            $this->coursesDetails->id_curso1 = $course->id_course;

            $crawler = $client->request('GET', "https://www.mastersportal.com/studies/$course->codigo_curso");

            $divDatos = $crawler->filter(" [id='StudyQuickFactsContainer']")->each(function ($nodeDiscipline) {
               $nodeDiscipline->filter("[class='js-duration']")->each(function ($nodeText) {
                  $duration = $nodeText->text();

                  $time = explode(" ", $duration);

                  $this->coursesDetails->fulltime_duration = $time[1];
                  $this->coursesDetails->duration_period = $time[2];
               });
            });

            $divLanguage = $crawler->filter("[class='Languages']")->each(function ($nodeLanguage) {
               $this->coursesDetails->language = $nodeLanguage->text();
            });


            $divAbout = $crawler->filter("[class='Module StudyPortals_Shared_Modules_Study_Description_Description']")->each(function ($nodeAbout) {
               $this->coursesDetails->full_description = $nodeAbout->text();
            });


            $linkU = $crawler->filter("[class='ChampionButton TrackingExternalLink StudyLink']")->each(function ($nodeLinkUniversity) {
               $this->coursesDetails->url_university = $nodeLinkUniversity->attr('href');
            });


            $divRequirement = $crawler->filter("[class='Module StudyPortals_Shared_Modules_Study_AcademicRequirements_AcademicRequirements']")->each(function ($nodeRequirement) {
               $this->coursesDetails->requirements = $nodeRequirement->text();
            });


            $this->coursesDetails->save();
         }

      } catch (\Exception $e) {
         return response()->json([
            "status3" => false,
            "error" => $e->getMessage()
         ]);
      }
   }
}
