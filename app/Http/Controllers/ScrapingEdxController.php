<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use App\{Disciplines, Courses, Institutions};

class ScrapingEdxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
         

    
        try {

            ini_set("memory_limit", "2048M");
            ini_set("max_execution_time", 10000);

            $arrayCourses=array();
            $client = new Client([
                'headers' => ['content-type'=>'application/json','Accept'=>'application/json'],
                
            ]);

            // echo "1";

            // $i = 0;

            for ($i = 1; $i < 5 ; $i++) {

            // while (true) {

                // echo $i;

                // $i++;

                try {

                    $response = $client->request('GET',"https://www.edx.org/api/v1/catalog/search?page=$i&page_size=1000");

                    $all_courses = json_decode($response->getBody());

                } catch (\Exception $e) {
                    break;
                }

                if ($response->getStatusCode() == 400 || empty($all_courses) || !isset($all_courses)) {
                    break;
                }
  
                foreach($all_courses->objects->results as $course){
                
                    array_push($arrayCourses, $course->marketing_url);
            
                }
           
                    // dd($arrayCourses);         
                foreach($arrayCourses as $course){
           
                    try{
                        $page_data = "page-data/";
                        $page_json = "/page-data.json";
                        $dominio_data = "https://www.edx.org/";
                        $url_final = explode("https://www.edx.org/", $course);
                        $url_course= $dominio_data.$page_data.$url_final[1].$page_json;
                        // dd($url_course);
                        try{
                            $course = $client->request('GET',$url_course);
                            // echo $course->getStatusCode();
                            $course = json_decode($course->getBody())->result;


                            /*
                            if (isset($course->data->primaryCourse->title)) {
                                $course_find = Courses::query()->where('name_course', '=', $course->data->primaryCourse->title)->first();

                                if (count($course_find) == 0) {
                                    break;
                                }
                            }
                            */


                            $discipline2 =null;
    
                            if(isset($course->data->primaryCourse->subjects[0]->name)){
                                $discipline2 = Disciplines::where("name_discipline","=",$course->data->primaryCourse->subjects[0]->name)->first();
                            }
                        
                            if ($discipline2 == null) {
                                
                                $discipline2 = new Disciplines();        
                                $discipline2->name_discipline =$course->data->primaryCourse->subjects[0]->name;
                                $discipline2->type = "Edx";
                                $discipline2->save();
        
                            }
    
                            $institution= null;
                            if (isset($course->data->primaryCourse->owners[0]->name)) {
                                $institution = Institutions::where("name_institution", "=", $course->data->primaryCourse->owners[0]->name)->first();
                            }
    
    
                        //  if (isset($course->venues[0]->city)) {
                        //     $city = Cities::where("name" , "=", $course->venues[0]->city)->first();
                        //  }
        
        
                            if ($institution == null) {
        
                                $institution = new Institutions();
                                $institution->name_institution = $course->data->primaryCourse->owners[0]->name;
                                $institution->save();
                            }
    
                            $course_create = new Courses();
                            if(isset($course->data->primaryCourse->enrollmentCount)){
                                $course_create->codigo_curso = $course->data->primaryCourse->enrollmentCount;
                            }
                            
    
                            $course_create->id_discipline = isset($discipline2->id_discipline) ? $discipline2->id_discipline : null;
                            $course_create->id_institution = isset($institution->id_institution) ? $institution->id_institution : null;
        
                            $course_create->name_course = isset($course->data->primaryCourse->title) ? $course->data->primaryCourse->title : null;
                            $course_create->description_course = isset($course->data->primaryCourse->short_description) ? $course->data->primaryCourse->short_description : null;
                            $course_create->type = "Edx";
                        //  $course_create->duration = isset($course->fulltime_duration->value) ? $course->fulltime_duration->value : null;
                            $course_create->url = isset($course->data->primaryCourse->image->src) ? $course->data->primaryCourse->image->src : null;
                            $course_create->density_parttime =true;
                            $course_create->density_fulltime =false;
                            $course_create->methods_face2face = false;
                            $course_create->methods_online = true;
                            $course_create->methods_blended = false;
                            $course_create->tuition_fee_value = isset($course->data->primaryCourse->entitlements[0]->price) ? $course->data->primaryCourse->entitlements[0]->price : null;
                            $course_create->tuition_fee_unit ="full";
                            $course_create->tuition_fee_currency = isset($course->data->primaryCourse->entitlements[0]->currency) ? $course->data->primaryCourse->entitlements[0]->currency : null;
                            $course_create->url_curso = $url_course;
        
                            $course_create->save(); 
                        }catch (\Exception $e) {
                            
                         }

                    }catch (\Exception $e) {
                        return response()->json([
                           "status" => false,
                           "error" => $e->getMessage()."a"
                        ]);
                     }          
                                           
                }
            }
                 
        } catch (\Exception $e) {
            return response()->json([
               "status" => false,
               "error" => $e->getMessage()."b"
            ]);
         }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
