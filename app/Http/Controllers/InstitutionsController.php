<?php

namespace App\Http\Controllers;

use App\Institutions;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstitutionsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
	public function index()
	{
		$institution = DB::table('institutions')
			->where('institutions.status', 'A')
			->limit(50)
			->get();

		$count = DB::table('institutions')
			->select('id_institution')
			->count();


		return response()->json([
			'institutions' => $institution,
			'count' => $count
		]);
	}


	public function getCountInstitutions(Request $request)
	{

		$institutions = DB::table('institutions')
			->offset(($request->all()['pageSize'] * $request->all()['pageIndex']))
			->orderBy('institutions.id_institution', 'DESC')
			->limit($request->all()['pageSize'])
			->get();


		return response()->json([
			'list_institutions' => $institutions
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return void
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function store(Request $request)
	{

		$url = null;

		if ($request->hasFile('image')) {

			$file = $request->file('image');
			$filename = $file->getClientOriginalName();
			$picture = date('His') . '-' . $filename;
			$file->move(storage_path('img_institution'), $picture);

			$url = storage_path('img_institution/' . $picture);
		}


		$institution = new Institutions();

		$institution->name_institution = $request['name_institution'];
		$institution->description_institution = $request['description_institution'];
		$institution->url = $url;


		$varResponse = $institution->save();


		return response()->json([
			'created_ins' => $varResponse
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return JsonResponse
	 */
	public function edit($id)
	{
		$institution = DB::table('institutions')
			->where('id_institution', $id)
			->get();

		return response()->json([
			'institution' => $institution
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse
	 */
	public function update(Request $request, $id)
	{
		Institutions::find($id)->update($request->all());

		return response()->json([
			'saved controller' => $id
		]);
	}

	public function institutionUpdateWidthImage(Request $request)
	{
		try {

			$url = null;

			if ($request->hasFile('image')) {

				$file = $request->file('image');
				$filename = $file->getClientOriginalName();
				$picture = date('His') . '-' . $filename;
				$file->move(storage_path('img_institution'), $picture);

				$url = storage_path('img_institution/' . $picture);
			}


			$institution = Institutions::find($request['id_institution']);

			$institution->name_institution = $request['name_institution'];
			$institution->description_institution = $request['description_institution'];
			$institution->url = $url;

			$institution->save();


			return response()->json([
				'updated' => true
			]);

		} catch (\Exception $e) {
			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return JsonResponse
	 */
	public function destroy($id)
	{
		$institution = Institutions::find($id);
		$institution->status = "I";
		$institution->save();

		return response()->json([
			"response" => $id
		]);
	}

	/**
	 * Read data width limit.
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function readLimit(Request $request)
	{
		ini_set('memory_limit', '2048M');
		ini_set('max_execution_time', 10000);


		$name_universities = null;

		if (isset($request->all()['filters']['name_universities'])) {
			$name_universities = $request->all()['filters']['name_universities'];
		}


		$universities = Institutions::query()
			->where('name_institution', 'LIKE', '' % $name_universities % '')
			->limit(50)
			->get();

		return response()->json([
			'universities' => $universities
		]);
	}

	public function contarInstituciones()
	{

		$userValida = DB::select("SELECT count(id_institution) institucion FROM institutions ");

		return response()->json([
			"status" => true,
			"message" => 'Continuar',
			"data" => $userValida
		]);


	}
}
