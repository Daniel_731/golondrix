<?php

namespace App\Http\Controllers;

use App\States;
use Illuminate\Http\Request;
use App\Cities;

class CitiesController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
      //
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
      //
   }

   /**
    * Display the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {

   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      $cities = Cities::query()
         ->where('state_id', $id)
         ->get();

      return response()->json([
         "cities" => $cities
      ]);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
      //
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      //
   }

   /**
    * Read data width limit.
    *
    * @param \Illuminate\Http\Request $request
    *
    * @return \Illuminate\Http\Response
    */
   public function readLimit($country_id)
   {
      $cities = Cities::query()
         ->where('country_id', $country_id)
         ->groupBy('name')
         ->get();

      return response()->json([
         "cities" => $cities
      ]);
   }


   /**
    * Show the form for editing the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function cityByState($id)
   {
      $cities = Cities::where('state_id', $id)->get();

      return response()->json([
         "cities" => $cities
      ]);
   }

}
