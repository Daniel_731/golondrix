<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Disciplines;
use Illuminate\Support\Facades\DB;

class DisciplinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        ini_set("memory_limit", "2048M");
        ini_set("max_execution_time", 10000);



        $discipline = Disciplines::query()->groupBy('name_discipline')->get();

        return response()->json([
            "discipline" => $discipline
        ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Disciplines::save($request->all());
         
         return response()->json([
            "america" => "$disciplines"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function getDisciplineForType($id)
    {
        $type = "";

        if($id == 1){
             $type = "Bachelor";
        }elseif ($id == 2) {
           $type = "Master";
        }elseif ($id == 3) {
            $type = "Phd";
        }

        $disciplines = DB::table('disciplines')->where('disciplines.type',$type)
        ->select('disciplines.id_discipline',
                 'disciplines.name_discipline',
                 'disciplines.type')
        ->get();

        return response()->json([
            "disciplines" => $disciplines
        ]);
    }
}
