<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Favorites;
use http\Env\Response;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
      //
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
      try {

         $favorite = new Favorite();

         $favorite->id_user = $request->all()['id_user'];
         $favorite->id_course = $request->all()['id_course'];

         $favorite->save();


         return response()->json([
            "status" => true,
            "id_favorite" => $favorite->id
         ]);

      } catch (\Exception $e) {

         return response()->json([
            "status" => false,
            "error" => $e->getMessage()
         ]);
      }
   }

   /**
    * Display the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      try {

         $favorites = Favorite::query()
            ->select(
               "favorite.id",
               "favorite.updated_at",
               "favorite.id_course AS favorite_id_course",
               "favorite.id_user",

               "courses.id_course AS courses_id_course",
               "courses.name_course",
               "courses.url",
               "courses.description_course",
               "courses.type",
               "courses.tuition_fee_value",
               "courses.tuition_fee_unit",
               "courses.tuition_fee_currency",
               "courses.density_fulltime",
               "courses.density_parttime",
               "courses.duration",
               "courses.methods_face2face",
               "courses.methods_online",
               "courses.methods_blended",
               "courses.url_curso",
               "courses.id_institution",

               "institutions.name_institution",
               "institutions.id_institution",

               "courses_details.url_university",
               "courses_details.id_curso1"
            )
            ->join('courses', 'favorite.id_course', '=', 'courses.id_course')
            ->leftJoin('courses_details', 'courses.id_course', '=', 'id_curso1')
            ->leftJoin('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
            ->where('favorite.id_user', '=', $id)
            ->orderBy('updated_at', 'DESC')
            ->get();


         return response()->json([
            "status" => true,
            "favorites" => $favorites
         ]);

      } catch (\Exception $e) {

         return response()->json([
            "status" => false,
            "error" => $e->getMessage()
         ]);
      }
   }

   /**
    * Update the specified resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
      //
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      Favorite::find($id)->delete();
   }
}
