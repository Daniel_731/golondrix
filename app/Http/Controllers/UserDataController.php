<?php

namespace App\Http\Controllers;

use App\UserData;
use App\UsuarioToken;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\LenguajeUser;
use App\PreviousUser;
use App\WorkExperience;
use App\TestUser;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class UserDataController extends Controller
{

	use AuthenticatesUsers;

	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
	public function index()
	{
		$user_data = UserData::all();

		return response()->json([
			"user_data" => $user_data
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */


	public function login(Request $request)
	{

		try {

			if (Auth::attempt(["email" => $request->email, "password" => $request->password])) {

				$user = UserData::where('email', $request->email)->first();

				$user->api_token = Str::random(60);
				$user->save();

				return response()->json([
					"status" => true,
					"api_token" => $user->api_token,
					"first_user" => isset($request["first_user"]) ? true : false
				]);

			}

			return response()->json([
				"status" => false,
				"error" => 'The username or password is incorrect'
			]);

		} catch (\Exception $e) {
			return response()->json([
				"status" => false,
				"error" => $e->getMessage()
			]);
		}

	}

	public function authenticate(Request $request)
	{

		$user = UserData::where('email', $request->email)->first();
		$credentials = $request->only('email', 'password');

		try {
			if (!$token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'invalid_credentials',
					'message' => 'Incorrect password'
				]);
			}
		} catch (JWTException $e) {
			return response()->json(['error' => 'could_not_create_token'], 500);
		}

		$user->api_token = $token;

		if ($user->save()) {
			$tokenUsu = $this->crearValidacionToken($user->id, $user->api_token);
		}

		return response()->json([
			"status" => true,
			"token" => $tokenUsu,
			"first_user" => isset($request["first_user"]) ? true : false
		]);

	}


	public function crearValidacionToken($id, $token)
	{

		// dd($token);

		$userValida = UsuarioToken::where('usuario_id', $id)->first();

		if ($userValida) {

			$userValida->token = $token;
			$userValida->token_fecha = date("Y-m-d");
			$userValida->token_hora = date("H:i:s");
			$userValida->save();

			$token = $userValida->token;
		} else {
			$usuarioToken = new UsuarioToken;
			$usuarioToken->token = $token;
			$usuarioToken->usuario_id = $id;
			$usuarioToken->token_fecha = date("Y-m-d");
			$usuarioToken->token_hora = date("H:i:s");
			$usuarioToken->save();
			$token = $usuarioToken->token;

		}

		return $token;
	}

	public function contarUsuarios()
	{

		$userValida = DB::select("SELECT count(id) usuarios FROM user_data ");

		return response()->json([
			"status" => true,
			"message" => 'Continuar',
			"data" => $userValida
		]);


	}

	public function listadoUsuariosMeses()
	{

		$userValida = DB::select("SELECT count(*) usuarios, created_at  FROM user_data group by month(created_at)");

		return response()->json([
			"status" => true,
			"message" => 'llego Fecha',
			"data" => $userValida
		]);


	}


	public function validaCaducidadToken(Request $request)
	{
		$idUser = $request[0];

		$userValida = DB::select("SELECT current_date as hora, token_fecha,
                           replace(token_hora,':','') AS token_hora,
                            token,usuario_id
                   FROM usuario_token  WHERE  usuario_id = {$idUser}");


		$fecha = date("Y-m-d");
		$hora = date("His");
		$fechaToken = $userValida[0]->token_fecha;
		$horaToken = $userValida[0]->token_hora;
		$user = $userValida[0]->usuario_id;
		$userToken = Str::random(60);


		$resultadoFecha = (($hora) - ($horaToken));


		if ($fecha != $fechaToken) {
			return response()->json([
				"status" => false,
				"message" => 'Please enter your password correctly',
				"token" => $userToken
			]);
		} else {

			if ($resultadoFecha <= 20000) {
				// $tokenUsu =  $this->crearValidacionToken($user, $userToken);

				return response()->json([
					"status" => true,
					"message" => 'Continuar',
					"token" => $userToken
				]);

			} else {
				// $tokenUsu =  $this->crearValidacionToken($user, $userToken);
				return response()->json([
					"status" => false,
					"message" => 'Please enter your password correctly',
					"token" => $userToken
				]);
			}
		}
	}
	public function loginRedSociale(Request $request)
	{
		try {

			$user = UserData::where('email', $request->email)->first();

			$request['password'] = $request['email'];

			$credentials = $request->only('email', 'password');

			try {
				if (!$token = JWTAuth::attempt($credentials)) {
					return response()->json(['error' => 'invalid_credentials',
						'message' => 'Incorrect password'
					]);
				}
			} catch (JWTException $e) {
				return response()->json(['error' => 'could_not_create_token'], 500);
			}

			$user->api_token = $token;

			if ($user->save()) {
				$tokenUsu = $this->crearValidacionToken($user->id, $user->api_token);
			}

			return response()->json([
				"status"       => true,
				"token"        => $tokenUsu,
				"api_token"    => $user->api_token,
				"first_user"   => isset($request["first_user"]) ? true : false
			]);

		} catch (\Exception $e) {
			return response()->json([
				"status" => false,
				"error" => $e->getMessage()
			]);
		}
	}

	public function store(Request $request)
	{
		try {

			$email = filter_var($request->email, FILTER_VALIDATE_EMAIL);

			if (!$email && $request['id_client'] == 1) {

				return response()->json([
					"status" => false,
					"error" => 2
				]);

			}

			$user = UserData::where('email', $email)->first();

			if ($user && $request['id_client'] == 1) {
				return response()->json([
					"status" => false,
					"error" => 1
				]);
			}

			if (!$user) {

				$api_token = Str::random(60);
				$password = $request["password"];
				if (isset($request['birth_day'])) {
					$request['birth_day'] = date_format(date_create($request['birth_day']), "Y-m-d");
				}

				$request["password"] = bcrypt($request["password"]);
				$request["api_token"] = $api_token;
				$request["email"] = $email;

				UserData::create($request->all());
				$request["password"] = $password;
				$request["first_user"] = true;

				$general_controller = new GeneralController();
				$general_controller->sendWelcomeEmail(
					$request->email,
					($request['id_client'] == 1 ? $request->user_name : $request->first_name)
				);
			}


			if ($request['id_client'] == 1) {
				return $this->login($request);
			} else {
				return $this->loginRedSociale($request);
			}

		} catch (\Exception $e) {
			return response()->json([
				"status" => false,
				"error" => $e->getMessage()
			]);
		}
	}

	public function userDataUniverscity(Request $request)
	{

		//dd($request);

		$message = "";
		$estatu = true;

		// try {

		$email = filter_var($request->email, FILTER_VALIDATE_EMAIL);

		if (!$email && $request['id_client'] == 1) {

			return response()->json([
				"status" => false,
				"error" => 2
			]);

		}

		$user = UserData::where('email', $email)->first();

		//   dd( $user );

		if (!$user) {

			$api_token = Str::random(60);
			$password = $request["password"];
			if (isset($request['birth_day'])) {
				$request['birth_day'] = date_format(date_create($request['birth_day']), "Y-m-d");
			}

			$request["password"] = bcrypt($request["password"]);
			$request["api_token"] = $api_token;
			$request["email"] = $email;

			$respuesta = UserData::create($request->all());
			if ($respuesta) {
				$message = "El usuario fue registrado Exitosamente";

			}

			$request["password"] = $password;
			$request["first_user"] = true;


		} else {
			$message = "No se pudo crear  usuario vueva a intentarlo";
			$estatu = false;
		}

		return response()->json([
			"status" => $estatu,
			"mensaje" => $message
		]);

		// } catch (\Exception $e) {
		//    return response()->json([
		//       "status" => false,
		//       "error" => $e->getMessage()
		//    ]);
		// }


	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return JsonResponse
	 */


	public function edit($id)
	{

		try {

			//$user_data = UserData::find($id);
			$courses = UserData::where('id', '=', $id)->get();

			foreach ($courses as $curs) {
				// echo "->". $curs;
				$curs['previous_studes'] = DB::table('previous_studes')->where('id_user', '=', $curs->id)->get();
				$curs['language_user'] = DB::table('language_user')->where('id_user', '=', $curs->id)->get();
				$curs['searching_detalle'] = DB::table('searching_detalle')->where('id_user', '=', $curs->id)->get();
				$curs['city'] = DB::table('cities')->where('id', '=', $curs->id_location)->get();

			}

			// dd($courses);

			return response()->json([
				"status" => true,
				"user_data" => $courses
			]);


		} catch (\Exception $e) {

			return response()->json([
				"status" => false,
				"error" => $e->getMessage()
			]);
		}

	}

	/**
	 * Detalles  adicionar datos del usuario
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function createPreviousStudes(Request $request)
	{
		try {

			PreviousUser::create($request->all());

			return response()->json([
				"status" => true
			]);

		} catch (\Exception $e) {
			return response()->json([
				"status" => false,
				"error" => $e->getMessage()
			]);
		}
	}

	public function readPreviousStudes($id)
	{
		$courses = DB::table('previous_studes')
			->leftJoin('countries', 'previous_studes.id_country', '=', 'countries.id')
			->select('previous_studes.id_previous',
				'previous_studes.university_user',
				'previous_studes.degree_user',
				'previous_studes.field_user',
				'previous_studes.end_user',
				'countries.name')
			->where('id_user', '=', $id)->get();

		return response()->json([
			"status" => true,
			"data" => $courses
		]);
	}

	public function deletePreviousStudes($id)
	{
		PreviousUser::find($id)->delete();

	}

	//LanguageUser

	public function createLanguageUser(Request $request)
	{
		LenguajeUser::create($request->all());
	}

	public function readLanguageUser($id)
	{
		$courses = LenguajeUser::where('id_user', '=', $id)->get();
		return response()->json([
			"status" => true,
			"data" => $courses
		]);
	}

	public function deleteLanguageUser($id)
	{
		LenguajeUser::find($id)->delete();

	}

	//WorkExperience
	public function createSearchingDetalle(Request $request)
	{


		$request['start_date'] = date_format(date_create($request['start_date']), "Y-m-d");
		$request['end_date'] = date_format(date_create($request['end_date']), "Y-m-d");


		WorkExperience::create($request->all());

	}

	public function readSearchingDetalle($id)
	{

		$experience = DB::table('work_experience')
			->leftJoin('countries', 'work_experience.id_countri', '=', 'countries.id')
			->select('countries.name',
				'work_experience.id',
				'work_experience.employer_name',
				'work_experience.job_title',
				'work_experience.start_date',
				'work_experience.end_date',
				'work_experience.id_countri',
				'work_experience.user_id',
				'work_experience.industry_job'
			)->where('work_experience.user_id', '=', $id)->get();

		return response()->json([
			"status" => true,
			"data" => $experience
		]);
	}


	public function deleteSearchingDetalle($id)
	{
		WorkExperience::find($id)->delete();

	}

	/*Test */
	public function createTestUser(Request $request)
	{
		TestUser::create($request->all());
	}

	public function readTestUser($id)
	{
		$test = TestUser::where('user_id', '=', $id)->get();
		return response()->json([
			"status" => true,
			"data" => $test
		]);
	}

	public function deleteTestUser($id)
	{
		TestUser::find($id)->delete();

	}

	// END  WorkExperience

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return void
	 */
	public function update(Request $request, $id)
	{
		if ($request['birth_day']) {
			$request['birth_day'] = date_format(date_create($request['birth_day']), "Y-m-d");
		}

		UserData::find($id)->update($request->all());
	}
 
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return void
	 */
	public function destroy($id)
	{
		UserData::find($id)->delete();
	}

	public function dataForExportUsers(Request $request){
		try{
		$dateMin = $request->all()['minDate'].' 00:00:00';
    	$dateMax = $request->all()['maxDate'].' 23:59:59';


    	$user = DB::table('user_data')
			->select(
					'user_data.id_client',
					'user_data.user_name',
					'user_data.email',
					'user_data.first_name',
					'user_data.last_name',
					'user_data.birth_day',
					'user_data.gender',
					'user_data.created_at',
					'user_data.gender',

				)
			->whereBetween('user_data.created_at',[$dateMin, $dateMax])
			->get();


       return response()->json([
			"data" => $user->all()
			
		]);
		}catch(Exception $e){
			return response()->json([
			"error" => $e
			
			]);
		}
	}
}
