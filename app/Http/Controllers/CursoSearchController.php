<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Favorite;
use App\History;
use App\Institutions;
use App\UserData;
use App\CoursesDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CursoSearchController extends Controller
{


   public function index(Request $request)
   {
      try {

         /*ini_set('memory_limit', '2048M');
         ini_set('max_execution_time', 10000);


         $field_data = null;
         $field_check = null;
         $field_fee = null;
         $page_ini = null;
         $id_city = null;
         $id_country = null;
         $id_institution = null;
         $skills = null;
         $type_course = null;
         $id_user = null;

         if (isset($request->all()['filters']['field_data'])) {
            $field_data = $request->all()['filters']['field_data'];
         }

         if (isset($request->all()['filters']['field_check'])) {
            $field_check = $request->all()['filters']['field_check'];
         }

         if (isset($request->all()['filters']['field_fee'])) {
            $field_fee = $request->all()['filters']['field_fee'];
         }

         if (isset($request->all()['filters']['initial_page'])) {
            $page_ini = $request->all()['filters']['initial_page'];
         }

         if (isset($request->all()['filters']['id_city'])) {
            $id_city = $request->all()['filters']['id_city'];
         }

         if (isset($request->all()['filters']['id_country'])) {
            $id_country = $request->all()['filters']['id_country'];
         }

         if (isset($request->all()['filters']['id_institution'])) {
            $id_institution = $request->all()['filters']['id_institution'];
         }

         if (isset($request->all()['filters']['skills'])) {
            $skills = $request->all()['filters']['skills'];
         }

         if (isset($request->all()['filters']['type_course'])) {
            $type_course = $request->all()['filters']['type_course'];
         }

         if (isset($request->all()['filters']['id_user'])) {
            $id_user = $request->all()['filters']['id_user'];
         }


         $courses = Courses::query()
            ->join('disciplines', 'courses.id_discipline', '=', 'disciplines.id_discipline')
            ->join('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
            ->leftJoin('cities', 'courses.id_city', '=', 'cities.id')
            ->leftJoin('courses_details', 'courses.id_course', '=', 'courses_details.id_curso1')
            ->leftJoin('favorites', 'courses.id_course', '=', 'favorites.id_curso2')
            ->leftJoin('user_data', 'favorites.id_usuario', '=', 'user_data.id')
            ->select(
               'disciplines.id_discipline',
               'disciplines.name_discipline',
               'disciplines.description_discipline',
               'institutions.id_institution',
               'institutions.name_institution',
               'institutions.url',
               'courses.id_course',
               'courses.name_course',
               'courses.description_course',
               'courses.type',
               'courses.duration',

               'courses.tuition_fee_value',
               'courses.tuition_fee_unit',
               'courses.tuition_fee_currency',

               'courses.methods_face2face',
               'courses.methods_online',
               'courses.methods_blended',

               'courses.city',
               'courses.country',
               'courses.area',

               'courses_details.id_curso1',
               'courses_details.url_curso',
               'courses_details.full_description',
               'courses_details.language',
               'courses_details.requirements',
               'courses_details.fulltime_duration',
               'courses_details.duration_period',
               'courses_details.required_level',
               'courses_details.experience',
               'courses_details.url_university',

               'courses.url as img_curse',
               'cities.name',

               'courses_details.full_description',
               'courses_details.requirements')
            ->addSelect(DB::raw('false as showSeeMore'));


         // ID user

         if ($id_user) {
            $courses = $courses->addSelect(DB::raw('(CASE WHEN user_data.id = $id_user THEN favorites.id ELSE NULL END) AS id_favorite'));
         }


         // Filters

         // Part time

         if ($field_check['partTime']) {
            $courses = $courses->where('courses.density_parttime', '=', $field_check['partTime']);
         }

         // Full time
         if ($field_check['fullTime']) {
            $courses = $courses->where('courses.density_fulltime', '=', $field_check['fullTime']);
         }

         // Degree Type

         $courses = $courses->where(function ($courses) use ($field_check) {
            if ($field_check['bachelor']) {
               $courses->where('courses.type', '=', 'Bachelor');
            }

            if ($field_check['master']) {
               $courses->orWhere('courses.type', '=', 'Master');
            }

            if ($field_check['phd']) {
               $courses->orWhere('courses.type', '=', 'Phd');
            }

            if ($field_check['shortCourse']) {
               $courses->orWhere('courses.type', '=', 'Edx');
               $courses->orWhere('courses.type', '=', 'Platzi');
            }

            if ($field_check['free']) {
               $courses->orWhere('courses.tuition_fee_value', '=', 0);
               $courses->orWhere('courses.tuition_fee_value', '=', null);
            }
         });

         // Mode

         $courses = $courses->where(function ($courses) use ($field_check) {
            if ($field_check['face']) {
               $courses->where('courses.methods_face2face', '=', true);
            }

            if ($field_check['online']) {
               $courses->orWhere('courses.methods_online', '=', true);
            }

            if ($field_check['blended']) {
               $courses->orWhere('courses.methods_blended', '=', true);
            }
         });

         // Fee

         if ($field_fee) {
            $courses = $courses->where('courses.tuition_fee_value', '<=', $field_fee);
         }


         // Duration

         $courses = $courses->where(function ($courses) use ($field_check) {
            if ($field_check['lessYear']) {
               $courses->where('courses.duration', '<=', 12);
            }

            if ($field_check['year1']) {
               $courses->orWhere('courses.duration', '=', 12);
            }

            if ($field_check['year2']) {
               $courses->orWhere('courses.duration', '=', 24);
            }

            if ($field_check['year4']) {
               $courses->orWhere('courses.duration', '=', 48);
            }
         });


         // Name or description

         if ($field_data) {
            $courses = $courses->where(function ($courses) use ($field_data) {
               $courses->where('disciplines.name_discipline', 'LIKE', '%$field_data%');
               $courses->orWhere('disciplines.description_discipline', 'LIKE', '%$field_data%');
            });
         }


         // City

         if ($id_city) {
            $courses = $courses->where('cities.id', '=', $id_city);
         }


         // Country

         if ($id_country) {
            $courses = $courses->where('cities.country_id', '=', $id_country);
         }


         // Institution

         if ($id_institution) {
            $courses = $courses->where('courses.id_institution', '=', $id_institution);
         }


         // Skills

         $skills_data = null;

         if ($skills) {
            $skills_data = preg_split('/( |, )/', $skills);

            $courses = $courses->where(function ($courses) use ($skills_data) {
               foreach ($skills_data as $skill) {
                  $courses->orWhere('courses.description_course', 'LIKE', '%$skill%');
               }
            });
         }


         // Type all or online courses

         if ($type_course) {
            $courses = $courses->where(function ($courses) use ($skills_data) {
               $courses = $courses->where('courses.type', '=', 'Platzi');
               $courses = $courses->OrWhere('courses.type', '=', 'Edx');
            });
         }


         $courses = $courses
            ->orderBy('courses.id_course', 'DESC')
            ->offset($page_ini)
            ->limit(25)
            ->get();


         return response()->json([
            'courses' => $courses
         ]);*/

         ini_set('memory_limit', '2048M');
         ini_set('max_execution_time', 10000);


         $field_data = null;
         $field_check = null;
         $field_fee = null;
         $page_ini = null;
         $name_country = null;
         $name_state = null;
         $name_city = null;
         $id_institution = null;
         $skills = null;
         $type_course = null;
         $id_user = null;
         $min_price = null;
         $max_price = null;
         $type_order = null;


         if (isset($request->all()['filters']['field_data'])) {
            $field_data = $request->all()['filters']['field_data'];
         }

         if (isset($request->all()['filters']['field_check'])) {
            $field_check = $request->all()['filters']['field_check'];
         }

         if (isset($request->all()['filters']['field_fee'])) {
            $field_fee = $request->all()['filters']['field_fee'];
         }

         if (isset($request->all()['filters']['initial_page'])) {
            $page_ini = $request->all()['filters']['initial_page'];
         }

         if (isset($request->all()['filters']['name_country'])) {
            $name_country = $request->all()['filters']['name_country'];
         }

         if (isset($request->all()['filters']['name_state'])) {
            $name_state = $request->all()['filters']['name_state'];
         }

         if (isset($request->all()['filters']['name_city'])) {
            $name_city = $request->all()['filters']['name_city'];
         }

         if (isset($request->all()['filters']['id_institution'])) {
            $id_institution = $request->all()['filters']['id_institution'];
         }

         if (isset($request->all()['filters']['skills'])) {
            $skills = $request->all()['filters']['skills'];
         }

         if (isset($request->all()['filters']['type_course'])) {
            $type_course = $request->all()['filters']['type_course'];
         }

         if (isset($request->all()['filters']['id_user'])) {
            $id_user = $request->all()['filters']['id_user'];
         }

         if (isset($request->all()['filters']['min_price'])) {
            $min_price = $request->all()['filters']['min_price'];
         }

         if (isset($request->all()['filters']['max_price'])) {
            $max_price = $request->all()['filters']['max_price'];
         }

         if (isset($request->all()['filters']['type_order'])) {
            $type_order = $request->all()['filters']['type_order'];
         }


         $courses = Courses::query()
            ->join('disciplines', 'disciplines.id_discipline', '=', 'courses.id_discipline')
            ->select('courses.*')
            ->with('discipline', 'institution');


         if ($id_user) {
            // $courses = $courses->addSelect(DB::raw("(CASE WHEN user_data.id = $id_user THEN favorites.id ELSE NULL END) AS id_favorite"));
         }


         // Filters

         // Part time

         if ($field_check['partTime']) {
            $courses = $courses->where('courses.density_parttime', '=', $field_check['partTime']);
         }

         
         // Full time

         if ($field_check['fullTime']) {
            $courses = $courses->where('courses.density_fulltime', '=', $field_check['fullTime']);
         }

         
         // Degree Type

         $courses = $courses->where(function ($courses) use ($type_course, $field_check) {
            if (!$type_course && $field_check['bachelor']) {
               $courses->orWhere('courses.type', '=', 'Bachelor');
            }

            if (!$type_course && $field_check['master']) {
               $courses->orWhere('courses.type', '=', 'Master');
            }

            if (!$type_course && $field_check['phd']) {
               $courses->orWhere('courses.type', '=', 'Phd');
            }

            if ($field_check['shortCourse']) {
               $courses->orWhere('courses.type', '=', 'Edx');
               $courses->orWhere('courses.type', '=', 'Platzi');
               $courses->orWhere('courses.duration', '<=', 6);
            }
         });


         // Mode

         $courses = $courses->where(function ($courses) use ($field_check) {
            if ($field_check['face']) {
               $courses->orWhere('courses.methods_face2face', '=', true);
            }

            if ($field_check['online']) {
               $courses->orWhere('courses.methods_online', '=', true);
            }

            if ($field_check['blended']) {
               $courses->orWhere('courses.methods_blended', '=', true);
            }
         });


         // Free

         if ($field_check['free']) {
            $courses = $courses->where(function ($courses) {
               $courses->orWhere('courses.tuition_fee_value', '=', 0);
               $courses->orWhere('courses.tuition_fee_value', '=', null);
            });
         }


         // Fee

         if (!$field_check['free'] && $field_fee) {
            $courses = $courses->where(function ($courses) use ($min_price, $max_price) {
               $courses->where('courses.tuition_fee_value', '>=', $min_price);
               $courses->where('courses.tuition_fee_value', '<=', $max_price);
            });
         }


         // Duration

         $courses = $courses->where(function ($courses) use ($field_check) {
            if ($field_check['lessYear']) {
               $courses->orWhere('courses.duration', '<=', 12);
            }

            if ($field_check['year1']) {
               $courses->orWhere('courses.duration', '=', 12);
            }

            if ($field_check['year2']) {
               $courses->orWhere('courses.duration', '=', 24);
            }

            if ($field_check['year4']) {
               $courses->orWhere('courses.duration', '=', 48);
            }
         });


         // Disciplines

         /*
         if ($field_data) {
            $courses = $courses->where(function ($courses) use ($field_data) {
               $courses->orWhere('courses.name_course', 'LIKE', "$field_data%");
               // $courses->orWhere('courses.description_course', 'LIKE', "%$field_data%");
            });
         }
         */

         $skills_data_disciplines = null;

         if ($field_data) {
            $skills_data_disciplines = preg_split('/( )/', $field_data);

            $courses = $courses->where(function ($courses) use ($skills_data_disciplines) {
               $courses = $courses->orWhere(function ($courses) use ($skills_data_disciplines) {
                  foreach ($skills_data_disciplines as $skill) {
                     $courses->where('courses.name_course', 'LIKE', "%$skill%");
                  }
               });

               if (count($skills_data_disciplines) > 1) {
                  $courses = $courses->orWhere(function ($courses) use ($skills_data_disciplines) {
                     foreach ($skills_data_disciplines as $skill) {
                        $courses->where('courses.description_course', 'LIKE', "%$skill%");
                     }
                  });
               }
            });
         }


         // Country

         if (!$type_course && $name_country) {
            $courses = $courses->where('country', 'LIKE', "%$name_country%");
         }


         // State

         if (!$type_course && $name_state) {
            $courses = $courses->where('area', 'LIKE', "%$name_state%");
         }


         // City

         if (!$type_course && $name_city) {
            $courses = $courses->where('city', 'LIKE', "%$name_city%");
         }


         // Institution

         if (!$type_course && $id_institution) {
            $courses = $courses->where('courses.id_institution', '=', $id_institution);
         }


         // Skills

         $skills_data = null;

         if ($skills) {
            $skills_data = preg_split('/(,)/', trim($skills));

            $courses = $courses->where(function ($courses) use ($skills_data) {
               foreach ($skills_data as $skill) {
                  $skill = trim($skill);

                  $courses->orWhere('courses.name_course', 'LIKE', "$skill%");
                  $courses->orWhere('courses.description_course', 'LIKE', "%$skill%");
               }
            });
         }


         // Type all or online courses

         if ($type_course) {
            $courses = $courses->where(function ($courses) use ($skills_data) {
               $courses = $courses->orWhere('courses.type', '=', 'Platzi');
               $courses = $courses->orWhere('courses.type', '=', 'Edx');
            });
         }


         // Count total records

         $count_results = clone $courses;

         $count_results = $count_results->select(DB::raw('COUNT(*) AS results'))->first();


         // Save SQL consult

         $sql = $courses->toSql();


         // Type order
         if ($field_check['type_order']) {
            $courses = $courses->where('courses.tuition_fee_value', '!=', null);
            $courses = $courses->orderBy('tuition_fee_value', $field_check['type_order']);
         }


         // Get data

         $courses = $courses
            ->offset($page_ini)
            ->groupBy('courses.name_course')
            ->groupBy('courses.codigo_curso')
            ->limit(25)
            ->get();


         foreach ($courses as $course) {

            // Favorite
            $favorite = Favorite::query()
               ->where('id_user', '=', $id_user)
               ->where('id_course', '=', $course->id_course)
               ->first();

            $course['id_favorite'] = ($favorite ? $favorite->id : null);


            // History
            $history = History::query()
               ->where('id_user', '=', $id_user)
               ->where('id_course', '=', $course->id_course)
               ->first();

            $course['id_history'] = ($history ? $history->id_history : null);


            // Course details
            $courses_details = CoursesDetails::query()
               ->where('id_curso1', '=', $course->id_course)
               ->first();

            $course['courses_details'] = ($courses_details ? $courses_details : null);
         }


         return response()->json([
            'courses'       => $courses,
            'count_results' => $count_results,
            // 'sql'           => $sql,
            // 'skills_data_disciplines' => $skills_data_disciplines
         ]);

      } catch (\Exception $e) {

         return response()->json([
            'status' => false,
            'error'  => $e->getMessage()
         ]);
      }
   }

}
