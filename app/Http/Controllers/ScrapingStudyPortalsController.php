<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ScrapingDetailBachelorController;
use Goutte\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as ClientGuzzle;
use App\{Cities, Disciplines, Courses, Institutions};


class ScrapingStudyPortalsController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

   public $disciplines;

   public $links;
   public $textCourse;
   public $linkCourses;
   public $totalLinks;
   public $idDiscipline;


   public function index()
   {

      $client = new Client();
      $this->disciplines = array();
      $this->links = array();

      $crawler = $client->request('GET', 'https://www.bachelorsportal.com/');
      $crawler->filter(" [id='DisciplineSpotlight']")->each(function ($nodeDiscipline) {

         $courses = $nodeDiscipline->children()->filter("[data-clickable='clickable']")->each(function ($nodeCourses) {

            $a = $nodeCourses->children()->filter("a")->each(function ($nodeA) {
               $this->links = $nodeA->attr('href');//Se tre el link para accder a el titulo y la descripción de la disciplina
            });

            $array1 = array(
               'discipline' => $nodeCourses->text(),
               'links' => $this->links
            );

            array_push($this->disciplines, $array1);
         });
      });

      $this->getScrapingCourseOfDiscipline($this->disciplines);
   }


   public function getScrapingCourseOfDiscipline($disciplineSelected)
   {

      $client2 = new Client();
      $this->textCourse = '';
      $this->linkCourses = '';
      $this->totalLinks = array();

      foreach ($disciplineSelected as $dis) {

         $this->idDiscipline = explode("/", $dis['links'])[4];
         $crawler2 = $client2->request('GET', $dis['links']);
         $this->textCourse = $crawler2->filter("[class='Module StudyPortals_Shared_Modules_Discipline_HeaderBase_HeaderBase StudyPortals_Bachelors_Modules_Discipline_Header_HeaderBachelors']");

         $link = $crawler2->filter("[class='DisciplineConversion']")->each(function ($nodeLinks) {
            $a = $nodeLinks->children()->filter("a")->each(function ($nodeA) {
               $this->totalLinks = array(
                  'total' => $nodeA->attr('href')
               );
            });
         });

         $crawler2->filter(" [class='Module StudyPortals_Shared_Modules_Discipline_DisciplineDescriptionRR_DisciplineDescriptionRR']")->each(function ($nodeDiscipline) {

            $courses = $nodeDiscipline->children()->filter("[id='DisciplineDescription']")->each(function ($nodeCourses) {
               $discipline = new Disciplines();
               
               $discipline->id_real = $this->idDiscipline;
               $discipline->name_discipline = $this->textCourse->text();
               $discipline->description_discipline = $nodeCourses->text();
               
               $discipline->save();
            });
         });

      }
   }


   public function getAllCourses()
   {

      try {

         ini_set("memory_limit", "2048M");
         ini_set("max_execution_time", 30000);

         // Bachelors

            // Agriculture & Forestry
            // [300, 301, 302, 303, 304, 305]

            // Applied Sciences & Professions
            // [297, 56, 132, 298, 112, 59, 60, 299, 321, 62]

            // Arts, Design & Architecture
            // [20, 259, 63, 260, 69, 261, 53, 262, 263, 104, 264, 318, 328, 265, 68]

            // Business & Management
            // [93, 325, 232, 233, 45, 234, 235, 236, 237, 322, 324, 86, 238, 87, 239, 111, 240, 241, 89, 88, 242, 243, 25, 101, 244, 245, 246, 133, 247, 248, 249]

            // Computer Science & IT
            // [330, 279, 281, 323, 282, 130, 283, 284, 285, 108, 331, 329, 280, 286]

            // Education & Training
            // [290, 291, 292, 333, 8, 293, 98, 294, 97, 295]

            // Engineering & Technology
            // [37, 250, 30, 32, 29, 34, 83, 94, 28, 251, 26, 252, 33, 39, 253, 254, 255, 256, 257]

            // Environmental Studies & Earth Sciences
            // [126, 125, 47, 314, 128, 287, 315, 123, 124, 288, 122, 127, 119]

            // Hospitality, Leisure & Sports
            // [312, 313, 311, 134, 317, 319]

            // Humanities
            // [266, 267, 268, 269, 74, 270, 77, 91, 271, 332, 272, 273, 84, 99]

            // Journalism & Media
            // [306, 307, 308, 309, 310]

            // Law
            // [219, 114, 109, 48, 85, 220, 221, 49, 115]

            // Medicine & Health
            // [31, 335, 228, 100, 229, 230, 92, 231, 113, 131, 106, 105, 90, 107]

            // Natural Sciences & Mathematics
            // [52, 82, 327, 326, 81, 118, 46, 223, 334, 40, 224, 225, 222, 226, 38, 227]

            // Social Sciences
            // [274, 72, 70, 67, 275, 71, 276, 110, 4, 73, 277, 76, 75, 102, 78, 103, 316, 79, 278, 80, 320]


         // Masters

            // Agriculture & Forestry
            // [300, 301, 302, 303, 304, 305]

            // Applied Sciences & Professions
            // [297, 56, 132, 298, 112, 59, 60, 299, 321, 62]

            // Arts, Design & Architecture
            // [20, 259, 63, 260, 69, 261, 53, 262, 263, 104, 264, 318, 328, 265, 68]

            // Business & Management
            // [93, 325, 232, 233, 45, 234, 235, 236, 237, 322, 324, 86, 238, 87, 239, 111, 240, 241, 89, 88, 242, 243, 25, 101, 244, 245, 246, 133, 247, 248, 249]

            // Computer Science & IT
            // [330, 279, 281, 323, 282, 130, 283, 284, 285, 108, 331, 329, 280, 286]

            // Education & Training
            // [290, 291, 292, 333, 8, 293, 98, 294, 97, 295]

            // Engineering & Technology
            // [37, 250, 30, 32, 29, 34, 83, 94, 28, 251, 26, 252, 33, 39, 253, 254, 255, 256, 257]

            // Environmental Studies & Earth Sciences
            // [126, 125, 47, 314, 128, 287, 315, 123, 124, 288, 122, 127, 119]

            // Hospitality, Leisure & Sports
            // [312, 313, 311, 134, 317, 319]

            // Humanities
            // [266, 267, 268, 269, 74, 270, 77, 91, 271, 332, 272, 273, 84, 99]

            // Journalism & Media
            // [306, 307, 308, 309, 310]

            // Law
            // [219, 114, 109, 48, 85, 220, 221, 49, 115]

            // Medicine & Health
            // [31, 335, 228, 100, 229, 230, 92, 231, 113, 131, 106, 105, 90, 107]

            // Natural Sciences & Mathematics
            // [52, 82, 327, 326, 81, 118, 46, 223, 334, 40, 224, 225, 222, 226, 38, 227]

            // Social Sciences
            // [274, 72, 70, 67, 275, 71, 276, 110, 4, 73, 277, 76, 75, 102, 78, 103, 316, 79, 278, 80, 320]


         // PhD

            // Agriculture & Forestry
            // [300, 301, 302, 303, 304, 305]

            // Applied Sciences & Professions
            // [297, 56, 132, 298, 112, 59, 60, 299, 321, 62]

            // Arts, Design & Architecture
            // [20, 259, 63, 260, 69, 261, 53, 262, 263, 104, 264, 318, 328, 265, 68]

            // Business & Management
            // [93, 325, 232, 233, 45, 234, 235, 236, 237, 322, 324, 86, 238, 87, 239, 111, 240, 241, 89, 88, 242, 243, 25, 101, 244, 245, 246, 133, 247, 248, 249]

            // Computer Science & IT
            // [330, 279, 281, 323, 282, 130, 283, 284, 285, 108, 331, 329, 280, 286]

            // Education & Training
            // [290, 291, 292, 333, 8, 293, 98, 294, 97, 295]

            // Engineering & Technology
            // [37, 250, 30, 32, 29, 34, 83, 94, 28, 251, 26, 252, 33, 39, 253, 254, 255, 256, 257]

            // Environmental Studies & Earth Sciences
            // [126, 125, 47, 314, 128, 287, 315, 123, 124, 288, 122, 127, 119]

            // Hospitality, Leisure & Sports
            // [312, 313, 311, 134, 317, 319]

            // Humanities
            // [266, 267, 268, 269, 74, 270, 77, 91, 271, 332, 272, 273, 84, 99]

            // Journalism & Media
            // [306, 307, 308, 309, 310]

            // Law
            // [219, 114, 109, 48, 85, 220, 221, 49, 115]

            // Medicine & Health
            // [31, 335, 228, 100, 229, 230, 92, 231, 113, 131, 106, 105, 90, 107]

            // Natural Sciences & Mathematics
            // [52, 82, 327, 326, 81, 118, 46, 223, 334, 40, 224, 225, 222, 226, 38, 227]

            // Social Sciences
            // [274, 72, 70, 67, 275, 71, 276, 110, 4, 73, 277, 76, 75, 102, 78, 103, 316, 79, 278, 80, 320]

         
         $id_real = [
            54    => [300, 301, 302, 303, 304, 305],
            12 	=> [297, 56, 132, 298, 112, 59, 60, 299, 321, 62],
            258   => [20, 259, 63, 260, 69, 261, 53, 262, 263, 104, 264, 318, 328, 265, 68],
            23 	=> [93, 325, 232, 233, 45, 234, 235, 236, 237, 322, 324, 86, 238, 87, 239, 111, 240, 241, 89, 88, 242, 243, 25, 101, 244, 245, 246, 133, 247, 248, 249],
            24 	=> [330, 279, 281, 323, 282, 130, 283, 284, 285, 108, 331, 329, 280, 286],
            289   => [290, 291, 292, 333, 8, 293, 98, 294, 97, 295],
            7 	   => [37, 250, 30, 32, 29, 34, 83, 94, 28, 251, 26, 252, 33, 39, 253, 254, 255, 256, 257],
            117   => [126, 125, 47, 314, 128, 287, 315, 123, 124, 288, 122, 127, 119],
            64 	=> [312, 313, 311, 134, 317, 319],
            9 	   => [266, 267, 268, 269, 74, 270, 77, 91, 271, 332, 272, 273, 84, 99],
            58 	=> [306, 307, 308, 309, 310],
            6 	   => [219, 114, 109, 48, 85, 220, 221, 49, 115],
            10 	=> [31, 335, 228, 100, 229, 230, 92, 231, 113, 131, 106, 105, 90, 107],
            11 	=> [52, 82, 327, 326, 81, 118, 46, 223, 334, 40, 224, 225, 222, 226, 38, 227],
            13 	=> [274, 72, 70, 67, 275, 71, 276, 110, 4, 73, 277, 76, 75, 102, 78, 103, 316, 79, 278, 80, 320]
         ];


         $disciplines = Disciplines::where("type", "=", "Phd")->get();

         foreach($disciplines as $discipline) {
            // echo $discipline->id_discipline . "<br />" . $discipline->id_real . "<br />" . print_r($id_real[$discipline->id_real]) . "<br /><br />";

            $all_courses = array();
            $response = null;

            $client = new ClientGuzzle([
               'headers' => ['content-type' => 'application/json', 'Accept' => 'application/json'],
            ]);

            foreach($id_real[$discipline->id_real] as $subdiscipline) {
               $i = 0;

               while (true) {
                  try {

                     try {

                        switch($discipline->type) {
                           case "Bachelor":
                              $response = $client->request("GET", "https://search.prtl.co/2018-07-23/?start=$i&q=di-$subdiscipline|en-3714|lv-bachelor|tc-USD|uc-88");
                              break;

                           case "Master":
                              $response = $client->request("GET", "https://search.prtl.co/2018-07-23/?start=$i&q=di-$subdiscipline%7Cen-2019%7Clv-master%7Ctc-USD%7Cuc-88");
                              break;

                           case "Phd":
                              $response = $client->request("GET", "https://search.prtl.co/2018-07-23/?start=$i&q=di-$subdiscipline%7Cen-1241%7Clv-phd%7Ctc-USD%7Cuc-88");
                              break;
                        }
                        
                        $all_courses = json_decode($response->getBody());
                        
                     } catch (\Exception $e) {
                        break;
                     }

                     if ($response->getStatusCode() == 400 || empty($all_courses) || !isset($all_courses)) {
                        break;
                     }

                     foreach ($all_courses as $course) {
                        try {

                           $discipline2 = Disciplines::where("id_discipline", "=", $discipline->id_discipline)->first();
                                    
                           if (isset($course->organisation_id)) {
                              $institution = Institutions::where("organisation_id", "=", $course->organisation_id)->first();
                           }

                           if (isset($course->venues[0]->city)) {
                              $city = Cities::where("name" , "=", $course->venues[0]->city)->first();
                           }

                           if ($institution == null) {
                              $institution = new Institutions();

                              $institution->organisation_id = $course->organisation_id;
                              
                              
                              if(isset($course->organisation)){
                                 $institution->name_institution = $course->organisation;
                              }
                              
                              if(isset($course->logo)){
                                 $institution->url = $course->logo;
                              }
                              
                              if(isset($course->summary)){
                                 $institution->description_institution = $course->summary;   
                              }

                              try {

                                 $institution->save();

                              } catch (\Exception $e) {}
                           }


                           $course_create = new Courses();

                           $course_create->id_discipline = isset($discipline2->id_discipline) ? $discipline2->id_discipline : null;
                           $course_create->id_institution = isset($institution->id_institution) ? $institution->id_institution : null;
                           $course_create->id_city = isset($city->id) ? $city->id : null;

                           $course_create->name_course = isset($course->title) ? $course->title : null;
                           $course_create->description_course = isset($course->summary) ? $course->summary : null;
                           $course_create->type = "Bachelor";
                           $course_create->duration = isset($course->fulltime_duration->value) ? $course->fulltime_duration->value : null;
                           $course_create->url = isset($course->logo) ? $course->logo : null;
                           $course_create->density_parttime = isset($course->density->parttime) ? $course->density->parttime : null;
                           $course_create->density_fulltime = isset($course->density->fulltime) ? $course->density->fulltime : null;
                           $course_create->methods_face2face = isset($course->methods->face2face) ? $course->methods->face2face : null;
                           $course_create->methods_online = isset($course->methods->online) ? $course->methods->online : null;
                           $course_create->methods_blended = isset($course->methods->blended) ? $course->methods->blended : null;
                           $course_create->tuition_fee_value = isset($course->tuition_fee->value) ? $course->tuition_fee->value : null;
                           $course_create->tuition_fee_unit = isset($course->tuition_fee->unit) ? $course->tuition_fee->unit : null;
                           $course_create->tuition_fee_currency = isset($course->tuition_fee->currency) ? $course->tuition_fee->currency : null;
                           $course_create->codigo_curso = isset($course->id) ? $course->id : null;
                           $course_create->city = isset($course->venues[0]->city) ? $course->venues[0]->city : null;
                           $course_create->country = isset($course->venues[0]->country) ? $course->venues[0]->country : null;
                           $course_create->area = isset($course->venues[0]->area) ? $course->venues[0]->area : null;

                           try {

                              $course_create->save();

                           } catch (\Exception $e) {}
                           
                           
                           
                        } catch (\Exception $e) {
                           return response()->json([
                              "status" => false,
                              "error-1" => $e->getMessage()
                           ]);
                        }
                     }

                     $i += 10;

                  } catch (\Exception $e) {
                     return response()->json([
                        "status" => false,
                        "error-2" => $e->getMessage()
                     ]);
                  }
               }  
            }
         }

      } catch (\Exception $e) {
         return response()->json([
            "status" => false,
            "error-3" => $e->getMessage()
         ]);
      }

   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      //
   }

   /**
    * Store a newly created resource in storage.
    *
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
      //
   }

   /**
    * Display the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
      //
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      //
   }
}