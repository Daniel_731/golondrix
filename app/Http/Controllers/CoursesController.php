<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Courses;
use App\CoursesDetails;
use App\UserData;

class CoursesController extends Controller
{


	public function index()
	{
		$courses = DB::table('courses')
			->join('courses_details', 'courses_details.id_curso1', '=', 'courses.id_course')
			->join('disciplines', 'courses.id_discipline', '=', 'disciplines.id_discipline')
			->leftJoin('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
			->leftJoin('cities', 'courses.id_city', '=', 'cities.id')
			->limit(10)
			->get();

		return response()->json([
			"courses" => $courses
		]);
	}

	public function coursesInstitution($id)
	{

		$user = UserData::where('id', '=', $id)->get();
		$typeUser = $user[0]['type_user'];

		if ($typeUser == 'Univercity') {
			$courses = DB::table('courses')
				->select(
					'courses.id_course',
					'courses.name_course',
					'courses.type',
					'institutions.name_institution'
				)
				->leftJoin('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
				->where('courses.user_id', $id)
				->where('courses.status', 'A')
				->where('courses.type', '!=', 'Edx')
				->orderBy('courses.id_course', 'DESC')
				->limit(50)
				->get();

			$allCourses = DB::table('courses')->where('user_id', $id)->count();

		} else {

			$courses = DB::table('courses')
				->select(
					'courses.id_course',
					'courses.name_course',
					'courses.type',
					'institutions.name_institution'
				)
				->leftJoin('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
				->where('courses.status', 'A')
				->where('courses.type', '!=', 'Edx')
				->orderBy('courses.id_course', 'DESC')
				->limit(50)
				->get();

			$allCourses = DB::table('courses')->count();
		}


		return response()->json([
			"length_courses" => $allCourses,
			"courses" => $courses
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{

		$id = 0;
		try {

			Courses::create($request->all());

			$courses = Courses::whereRaw('id_course = (select max(`id_course`) from courses)')->get();
			$details = $request->all()['coursedetail'];

			foreach ($courses as $c) {
				$id = $c->id_course;
			}

			$details['id_curso1'] = $id;
			CoursesDetails::create($details);

		} catch (\Exception $e) {
			return response()->json([
				"Error" => $e
			]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function edit($id)
	{
		$cursos = DB::table('courses')
			->select('disciplines.id_discipline',
				'disciplines.name_discipline',
				'disciplines.description_discipline',
				'institutions.id_institution',
				'institutions.name_institution',
				'institutions.url',
				'courses.id_course',
				'courses.name_course',
				'courses.description_course',
				'courses.type',
				'courses.duration',
				'courses.tuition_fee_value',
				'courses.density_parttime',
				'courses.density_fulltime',
				'courses.tuition_fee_unit',
				'courses.tuition_fee_currency',
				'courses.url as img_curse',
				'courses.city',
				'courses.country',
				'courses.methods_face2face',
				'courses.methods_online',
				'courses.methods_blended',
				'courses.area',
				'cities.name'
			)
			->join('disciplines', 'courses.id_discipline', '=', 'disciplines.id_discipline')
			->leftJoin('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
			->leftJoin('cities', 'courses.id_city', '=', 'cities.id')
			->where('courses.id_course', $id)
			->orderBy('courses.id_course', 'DESC')
			->get();

		return response()->json([
			"courses" => $cursos
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Request $request, $id)
	{
		try {

			Courses::find($id)->update($request->all());

			$data = [
				'id_curso1'          => $request->all()['coursedetail']['id_curso1'],
				'id_detail'          => $request->all()['coursedetail']['id_detail'],
				'url_curso'          => $request->all()['coursedetail']['url_curso'],
				'full_description'   => $request->all()['coursedetail']['full_description'],
				'language'           => $request->all()['coursedetail']['language'],
				'requirements'       => $request->all()['coursedetail']['requirements'],
				'fulltime_duration'  => $request->all()['coursedetail']['fulltime_duration'],
				'duration_period'    => $request->all()['coursedetail']['duration_period'],
				'required_level'     => $request->all()['coursedetail']['required_level'],
				'experience'         => $request->all()['coursedetail']['experience'],
				'url_university'     => $request->all()['coursedetail']['url_university'],
			];


			DB::table('courses_details')
				->where('id_detail', $data['id_detail'])
				->update($data);

		} catch (\Exception $e) {
			return response()->json([
				"Error" => $e
			]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function destroy($id)
	{
		$course = Courses::find($id);
		$course->status = "I";
		$course->save();

		return response()->json([
			"response" => $id
		]);
	}

	public function getAllCourses(Request $request)
	{

		$user = UserData::where('id', '=', $request['datos'])->get();
		$typeUser = $user[0]['type_user'];

		if ($typeUser == 'Univercity') {

			$courses = DB::table('courses')
				->select(
					'courses.id_course',
					'courses.name_course',
					'courses.type',
					'institutions.name_institution'
				)
				->leftJoin('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
				->where('user_id', '=', $request['datos'])
				->where('courses.type', '!=', 'Edx')
				->orderBy('courses.id_course', 'DESC')
				->limit($request['pageSize'])
				->offset($request['pageSize'] * $request['pageIndex'])
				->get();

		} else {

			$courses = DB::table('courses')
				->select(
					'courses.id_course',
					'courses.name_course',
					'courses.type',
					'institutions.name_institution'
				)
				->leftJoin('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
				->where('courses.type', '!=', 'Edx')
				->orderBy('courses.id_course', 'DESC')
				->limit($request['pageSize'])
				->offset($request['pageSize'] * $request['pageIndex'])
				->get();
		}


		return response()->json([
			"list_courses" => $courses
		]);
	}


	public function contarCursos()
	{
		$userValida = DB::select("SELECT count(id_course) course FROM courses ");

		return response()->json([
			"status"    => true,
			"message"   => 'Continuar',
			"data"      => $userValida
		]);
	}

}
