<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Institutions;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class GeneralController extends Controller
{

	/**
	 * Return only email view
	 *
	 * @param $email
	 * @param $firstName
	 *
	 * @return JsonResponse
	 */
	public function sendWelcomeEmail($email, $firstName)
	{
		try {

			ini_set('max_execution_time', 300);


			$template = [
				'name' => $firstName
			];


			Mail::send('templates.registerEmail', $template, function ($message) use ($email) {
				$message->from('welcome@golondrix.com', 'Golondrix');
				$message->to($email, '');
				$message->subject('Welcome to Golondrix!');
			});


			return response()->json([
				'success' => true
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}

	/**
	 * Send mail with contact form
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function sendContactEmail(Request $request)
	{
		try {

			ini_set('max_execution_time', 300);


			$name = $request->all()['name'];
			$email = $request->all()['email'];
			$comments = $request->all()['comments'];


			$body_email_data = [
				'email' => $email,
				'comments' => $comments
			];


			Mail::send('templates.contactFormEmail', $body_email_data, function ($message) use ($name, $email) {
				$message->from('welcome@golondrix.com', $name);
				$message->to($email, '');
				$message->subject('Golondrix contact form');
			});


			return response()->json([
				'status' => true
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}

	/**
	 * Send mail with contact form
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function helpMeToApplyEmail(Request $request)
	{
		try {

			ini_set('max_execution_time', 300);


			$name = $request['name'];


			$body_email_data = [
				'name'         => $request['name'],
				'email'        => $request['email'],
				'nationality'  => $request['nationality'],
				'course'       => $request['course'],
				'institution'  => $request['institution'],
				'interests'    => $request['comments']
			];


			Mail::send('templates.helpMeToApplyEmail', $body_email_data, function ($message) use ($name) {
				$message->from('welcome@golondrix.com', $name);
				$message->to('dsb.php@gmail.com', '');
				$message->subject('Golondrix help me to apply');
			});


			return response()->json([
				'status' => true
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}

	/**
	 * Charge institutions url page
	 *
	 * @return JsonResponse
	 */
	public function chargeInstitutionsURL()
	{
		try {

			$data = Courses::query()
				->join('courses_details', 'courses.id_course', '=', 'courses_details.id_curso1')
				->join('institutions', 'courses.id_institution', '=', 'institutions.id_institution')
				->select('institutions.id_institution', 'courses_details.url_university', 'courses.codigo_curso')
				->whereNotNull('url_university')
				->groupBy('id_institution')
				->get();


			foreach ($data as $item) {
				$institution = Institutions::find($item->id_institution);

				if ($institution) {
					$institution->url = $item->url_university;
					$institution->update();
				}
			}


			return response()->json([
				'status' => true
			]);

		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'error' => $e->getMessage()
			]);
		}
	}

}
