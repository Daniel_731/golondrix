<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_client
 * @property int $id_location
 * @property string $employer_name
 * @property string $job_title
 * @property string $start_date
 * @property UserDatum $userDatum
 * @property Location $location
 */
class WorkExperience extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'work_experience';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['id_client', 'id_location', 'employer_name','industry_job', 'job_title', 'start_date','user_id','id_countri','end_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location', 'id_location', 'id_location');
    }
}
