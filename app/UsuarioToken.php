<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $usutok_id
 * @property int $usuario_id
 * @property string $token
 * @property string $token_fecha
 * @property string $token_hora
 * @property UserDatum $userDatum
 */
class UsuarioToken extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'usuario_token';
    public $timestamps = false;
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'usutok_id';

    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'token', 'token_fecha', 'token_hora'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'usuario_id');
    }
}
