<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_history
 * @property int $id_user
 * @property int $id_course
 * @property string $created_at
 * @property string $updated_at
 * @property UserData $userData
 * @property Courses $course
 */
class History extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'history';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_history';

    /**
     * @var array
     */
    protected $fillable = ['id_history', 'id_user', 'id_course', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserData', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('App\Courses', 'id_course', 'id_course');
    }
}
