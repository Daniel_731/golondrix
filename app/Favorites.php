<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_usuario
 * @property int $id_curso2
 * @property string $created_at
 * @property string $updated_at
 * @property UserDatum $userDatum
 * @property Course $course
 */
class Favorites extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_usuario', 'id_curso2', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'id_usuario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('App\Course', 'id_curso2', 'id_course');
    }
}
