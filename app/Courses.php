<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_course
 * @property int $id_discipline
 * @property int $id_institution
 * @property int $id_city
 * @property string $name_course
 * @property string $description_course
 * @property string $type
 * @property string $category
 * @property string $duration
 * @property string $url
 * @property string $status
 * @property boolean $density_parttime
 * @property boolean $density_fulltime
 * @property boolean $methods_face2face
 * @property boolean $methods_online
 * @property boolean $methods_blended
 * @property string $created_at
 * @property string $updated_at
 * @property float $tuition_fee_value
 * @property string $tuition_fee_unit
 * @property string $tuition_fee_currency
 * @property int $codigo_curso
 * @property string $url_curso
 * @property string $city
 * @property string $country
 * @property string $area
 * @property Discipline $discipline
 * @property Institution $institution
 * @property CoursesDetail[] $coursesDetails
 * @property Favorite[] $favorites
 * @property History[] $histories
 */
class Courses extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_course';

    /**
     * @var array
     */
    protected $fillable = ['id_discipline', 'id_institution', 'id_city', 'name_course', 'description_course', 'type', 'category', 'duration', 'url', 'status', 'density_parttime', 'density_fulltime', 'methods_face2face', 'methods_online', 'methods_blended', 'created_at', 'updated_at', 'tuition_fee_value', 'tuition_fee_unit', 'tuition_fee_currency', 'codigo_curso', 'url_curso', 'city', 'country', 'area','user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discipline()
    {
        return $this->belongsTo('App\Disciplines', 'id_discipline', 'id_discipline');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function institution()
    {
        return $this->belongsTo('App\Institutions', 'id_institution', 'id_institution');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Cities', 'id_city');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coursesDetails()
    {
        return $this->hasMany('App\CoursesDetail', 'id_curso1', 'id_course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favorites()
    {
        return $this->hasMany('App\Favorite', 'id_course', 'id_course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this->hasMany('App\History', 'id_course', 'id_course');
    }
}
