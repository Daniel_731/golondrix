<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $iso3
 * @property string $iso2
 * @property string $phonecode
 * @property string $capital
 * @property string $currency
 * @property string $created_at
 * @property string $updated_at
 * @property City[] $cities
 * @property Location[] $locations
 * @property State[] $states
 */
class Countries extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'iso3', 'iso2', 'phonecode', 'capital', 'currency', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locations()
    {
        return $this->hasMany('App\Location', 'id_country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany('App\State');
    }
}
