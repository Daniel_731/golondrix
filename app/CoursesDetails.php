<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_curso1
 * @property int $id_detail
 * @property string $url_curso
 * @property string $full_description
 * @property string $language
 * @property string $requirements
 * @property int $fulltime_duration
 * @property string $duration_period
 * @property string $required_level
 * @property string $experience
 * @property string $url_university
 * @property string $created_at
 * @property string $updated_at
 * @property Course $course
 */
class CoursesDetails extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_detail', 'id_curso1', 'url_curso', 'full_description', 'language', 'requirements', 'fulltime_duration', 'duration_period', 'required_level', 'experience', 'url_university', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('App\Course', 'id_curso1', 'id_course');
    }
}
