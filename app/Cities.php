<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $state_id
 * @property int $country_id
 * @property string $created_at
 * @property string $updated_on
 * @property Course[] $courses
 */
class Cities extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'state_id', 'country_id', 'created_at', 'updated_on'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses()
    {
        return $this->hasMany('App\Course', 'id_city');
    }
}
