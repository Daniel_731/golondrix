<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_language
 * @property int $id_user
 * @property string $language_name
 * @property string $language_level
 * @property UserDatum $userDatum
 */
class LenguajeUser extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'language_user';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_language';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'language_name', 'language_level'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'id_user');
    }
}
