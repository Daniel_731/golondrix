<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id_favorite
 * @property int $id_course
 * @property int $id_user
 * @property string $created_at
 * @property string $updated_at
 * @property Courses $course
 * @property UserData $userData
 */
class Favorite extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'favorite';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = ['id', 'id_course', 'id_user', 'created_at', 'updated_at'];

    /**
     * @return BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('App\Courses', 'id_course', 'id_course');
    }

    /**
     * @return BelongsTo
     */
    public function userData()
    {
        return $this->belongsTo('App\UserData', 'id_user', 'id');
    }
}
