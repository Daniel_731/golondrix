<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_client
 * @property int $id_location
 * @property string $school_name
 * @property string $degree_status
 * @property string $major_area_of_study
 * @property string $gpa
 * @property string $sat
 * @property string $description_profile
 * @property UserDatum $userDatum
 * @property Location $location
 */
class HigherEducation extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'higher_education';

    /**
     * @var array
     */
    protected $fillable = ['id_client', 'id_location', 'school_name', 'degree_status', 'major_area_of_study', 'gpa', 'sat', 'description_profile'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location', 'id_location', 'id_location');
    }
}
