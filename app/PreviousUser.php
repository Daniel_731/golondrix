<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_previous
 * @property int $id_user
 * @property int $id_country
 * @property string $university_user
 * @property string $degree_user
 * @property string $field_user
 * @property int $end_user
 * @property Country $country
 * @property UserDatum $userDatum
 */
class PreviousUser extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'previous_studes';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_previous';
    public $timestamps = false;


    /**
     * @var array
     */
    protected $fillable = ['id_user', 'id_country', 'university_user', 'degree_user', 'field_user', 'end_user'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'id_country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'id_user');
    }
}
