<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_discipline
 * @property int $score
 * @property string $comment
 * @property string $date_time
 * @property int $id_client
 * @property Discipline $discipline
 */
class ScoreDisciplines extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_discipline', 'score', 'comment', 'date_time', 'id_client'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discipline()
    {
        return $this->belongsTo('App\Discipline', 'id_discipline', 'id_discipline');
    }
}
