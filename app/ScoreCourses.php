<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_course
 * @property int $score
 * @property string $comment
 * @property string $date_time
 * @property int $id_client
 * @property Course $course
 */
class ScoreCourses extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_course', 'score', 'comment', 'date_time', 'id_client'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('App\Course', 'id_course', 'id_course');
    }
}
