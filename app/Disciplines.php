<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_discipline
 * @property int $id_real
 * @property int $id_institution
 * @property string $name_discipline
 * @property string $description_discipline
 * @property string $type
 * @property string $category
 * @property string $duration
 * @property string $method
 * @property string $density
 * @property string $status
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 * @property Course[] $courses
 */
class Disciplines extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_discipline';

    /**
     * @var array
     */
    protected $fillable = ['id_real', 'id_institution', 'name_discipline', 'description_discipline', 'type', 'category', 'duration', 'method', 'density', 'status', 'url', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses()
    {
        return $this->hasMany('App\Courses', 'id_discipline', 'id_discipline');
    }
}
