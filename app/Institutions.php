<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id_institution
 * @property string $name_institution
 * @property string $description_institution
 * @property int $id_location
 * @property string $type
 * @property string $status
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 * @property int $organisation_id
 * @property Courses[] $courses
 */
class Institutions extends Model
{
	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id_institution';

	/**
	 * @var array
	 */
	protected $fillable = ['name_institution', 'description_institution', 'id_location', 'type', 'status', 'url', 'created_at', 'updated_at', 'organisation_id'];

	/**
	 * @return HasMany
	 */
	public function courses()
	{
		return $this->hasMany('App\Course', 'id_institution', 'id_institution');
	}
}
