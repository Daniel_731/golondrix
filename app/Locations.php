<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_location
 * @property int $id_country
 * @property string $names
 * @property Country $country
 * @property HigherEducation[] $higherEducations
 * @property Institution[] $institutions
 */
class Locations extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_location';

    /**
     * @var array
     */
    protected $fillable = ['id_country', 'names'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'id_country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function higherEducations()
    {
        return $this->hasMany('App\HigherEducation', 'id_location', 'id_location');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function institutions()
    {
        return $this->hasMany('App\Institution', 'id_location', 'id_location');
    }
}
