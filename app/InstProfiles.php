<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_institution
 * @property string $description
 * @property int $id_location
 * @property string $type
 * @property Institution $institution
 */
class InstProfiles extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_institution', 'description', 'id_location', 'type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function institution()
    {
        return $this->belongsTo('App\Institution', 'id_institution', 'id_institution');
    }
}
