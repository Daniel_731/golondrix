<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $type_test
 * @property string $score
 * @property UserDatum $userDatum
 */
class TestUser extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'test';

    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'type_test', 'score','type_test2','core2'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'user_id');
    }
}
