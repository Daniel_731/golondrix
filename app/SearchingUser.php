<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_searching
 * @property int $id_user
 * @property string $level_user
 * @property UserDatum $userDatum
 */
class SearchingUser extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'searching_detalle';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_searching';

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'level_user'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDatum()
    {
        return $this->belongsTo('App\UserDatum', 'id_user');
    }
}
