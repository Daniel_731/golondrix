<?php

namespace App\Exports;

use App\UserData;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
class UserExportData implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

    	$dateMin = '2019-09-18 00:00:00';
    	$dateMax = '2019-12-18 00:00:00';


    	$user = DB::table('user_data')
			->select(
					'user_data.id_client',
					'user_data.user_name',
					'user_data.email',
					'user_data.first_name',
					'user_data.last_name',
					'user_data.birth_day',
					'user_data.gender',
					'user_data.created_at',
					'user_data.gender',

				)
			->whereBetween('user_data.created_at', [$dateMin, $dateMax])
			->get();


        return $user;
    }
}
