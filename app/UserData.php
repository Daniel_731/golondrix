<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use phpDocumentor\Reflection\Location;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


/**
 * @property int $id
 * @property int $id_location
 * @property int $id_client
 * @property string $user_name
 * @property string $email
 * @property string $password
 * @property string $avatar
 * @property string $first_name
 * @property string $last_name
 * @property string $birth_day
 * @property string $gender
 * @property string $status
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Location $location
 * @property HigherEducation[] $higherEducations
 * @property History[] $histories
 * @property WorkExperience[] $workExperiences
 */
class UserData extends Authenticatable implements JWTSubject
{

    use Notifiable;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $table = 'user_data';

    /**
     * @var array
     */
    protected $fillable = ['user_name', 'id_client','email', 'password', 'avatar', 'first_name', 'last_name', 'api_token', 'created_at', 'updated_at','biography_users','birth_day','gender','study_year','user_bachelor','user_master','user_phd','user_chortcourse', 'id_location','type_user'];

   /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
   protected $hidden = ['password', 'api_token'];


   public function getJWTIdentifier()
   {
       return $this->getKey();
   }
   public function getJWTCustomClaims()
   {
       return [];
   }
   
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location', 'id_location', 'id_location');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function higherEducations()
    {
        return $this->hasMany('App\HigherEducation', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this->hasMany('App\History', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workExperiences()
    {
        return $this->hasMany('App\WorkExperience', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany('App\Favorites');
    }
}
